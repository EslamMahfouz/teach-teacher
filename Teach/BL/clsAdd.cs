﻿using System;
using Teach.EDM;

namespace Teach.BL
{
    class clsAdd
    {
        TeachEntities db = new TeachEntities();

        public void addAbsence(int idStudent)
        {
            var student = db.Students.Find(idStudent);
            double price = Convert.ToDouble(student.Price);
            Absence abs = new Absence()
            {
                idStudent = idStudent,
                date = DateTime.Now.Date,
                day1 = false,
                day2 = false,
                day3 = false,
                day4 = false,
                day5 = false,
                day6 = false,
                day7 = false,
                day8 = false,
                day9 = false,
                day10 = false,
                day11 = false,
                day12 = false,
                day13 = false,
                day14 = false,
                Price = price,
                Paid = 0,
                Carry = price,
                Exam = 0,
            };
            db.Absences.Add(abs);
            db.SaveChanges();
        }
    }
}
