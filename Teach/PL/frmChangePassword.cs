﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Teach.EDM;
namespace Teach.PL
{
    public partial class frmChangePassword : XtraForm
    {
        TeachEntities db = new TeachEntities();
        public int userID;

        public frmChangePassword()
        {
            InitializeComponent();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (!valPassword.Validate())
            {
                txtPassword.Focus();
                return;
            }
            if (!valConfirm.Validate())
            {
                txtConfirm.Focus();
                return;
            }
            var user = db.Users.Find(userID);
            user.password = txtPassword.Text;
            db.SaveChanges();
            XtraMessageBox.Show("تم تغيير الرقم السري", "تم", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DialogResult = DialogResult.OK;
        }
    }
}