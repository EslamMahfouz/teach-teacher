﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Teach.EDM;
using System.Data.Sql;
using System.Data;
using Microsoft.SqlServer.Management.Smo;

namespace Teach.PL
{
    public partial class frmLogin : XtraForm
    {
        private TeachEntities db = new TeachEntities();
        private bool _altF4Pressed;

        public frmLogin()
        {
            InitializeComponent();
            ActiveControl = txtUserName;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var userName = txtUserName.Text;
            var password = txtPassword.Text;
            var log = (from x in db.Users
                       where x.userName == userName && x.password == password
                       select x).ToList();

            if (log.Count > 0)
            {
                foreach (var item in log)
                {
                    Program.username = item.userName;
                    Program.addStudent = Convert.ToBoolean(item.UsersAccess.AddStudent);
                    Program.transferStudent = Convert.ToBoolean(item.UsersAccess.TransferStudent);
                    Program.addGroup = Convert.ToBoolean(item.UsersAccess.AddGroup);
                    Program.editGroup = Convert.ToBoolean(item.UsersAccess.EditGroup);
                    Program.addUser = Convert.ToBoolean(item.UsersAccess.AddUser);
                    Program.editUser = Convert.ToBoolean(item.UsersAccess.EditUser);
                    Program.paidAndAbsence = Convert.ToBoolean(item.UsersAccess.AbsenceAndPaid);
                    Program.reports = Convert.ToBoolean(item.UsersAccess.Reports);
                    break;
                }
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("من فضلك أدخل بيانات صحيحة", "خطأ في البيانات", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
