﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Teach.EDM;

namespace Teach.PL
{
    public partial class frmEditUser : XtraForm
    {
        TeachEntities db = new TeachEntities();
        int userID;
        void fillUser()
        {
            var user = db.Users.Find(userID);
            NameTextEdit.Text = user.Name;
            AddressTextEdit.Text = user.Address;
            PhoneTextEdit.Text = user.Phone;
            TelephoneTextEdit.Text = user.Telephone;
            AddStudentCheckEdit.EditValue = user.UsersAccess.AddStudent;
            TransferStudentCheckEdit.EditValue = user.UsersAccess.TransferStudent;
            AddGroupCheckEdit.EditValue = user.UsersAccess.AddGroup;
            EditGroupCheckEdit.EditValue = user.UsersAccess.EditGroup;
            AddUserCheckEdit.EditValue = user.UsersAccess.AddUser;
            EditUserCheckEdit.EditValue = user.UsersAccess.EditUser;
            AbsenceAndPaidCheckEdit.EditValue = user.UsersAccess.AbsenceAndPaid;
            ReportsCheckEdit.EditValue = user.UsersAccess.Reports;
        }
        void updateUser()
        {
            var user = db.Users.Find(userID);
            user.Name = NameTextEdit.Text;
            user.Address = AddressTextEdit.Text;
            user.Phone = PhoneTextEdit.Text;
            user.Telephone = TelephoneTextEdit.Text;
            user.UsersAccess.AddStudent = Convert.ToBoolean(AddStudentCheckEdit.EditValue);
            user.UsersAccess.TransferStudent = Convert.ToBoolean(TransferStudentCheckEdit.EditValue);
            user.UsersAccess.AddGroup = Convert.ToBoolean(AddGroupCheckEdit.EditValue);
            user.UsersAccess.EditGroup = Convert.ToBoolean(EditGroupCheckEdit.EditValue);
            user.UsersAccess.AddUser = Convert.ToBoolean(AddUserCheckEdit.EditValue);
            user.UsersAccess.EditUser = Convert.ToBoolean(EditUserCheckEdit.EditValue);
            user.UsersAccess.AbsenceAndPaid = Convert.ToBoolean(AbsenceAndPaidCheckEdit.EditValue);
            user.UsersAccess.Reports = Convert.ToBoolean(ReportsCheckEdit.EditValue);
            db.SaveChanges();
        }
        public frmEditUser()
        {
            InitializeComponent();
        }

        private void frmEditUser_Load(object sender, EventArgs e)
        {
            var users = from x in db.Users
                        select new { م = x.UserID, الإسم = x.userName };
            cmbUsers.Properties.DataSource = users.ToList();
            cmbUsers.Properties.PopulateColumns();
            cmbUsers.Properties.DisplayMember = "الإسم";
            cmbUsers.Properties.ValueMember = "م";
            cmbUsers.Properties.Columns["م"].Visible = false;
        }

        private void cmbUsers_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                btnSave.Enabled = true;
                userID = Convert.ToInt32(cmbUsers.EditValue);
                fillUser();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!valName.Validate())
                {
                    NameTextEdit.Focus();
                    return;
                }
                if (!valPhone.Validate())
                {
                    PhoneTextEdit.Focus();
                    return;
                }
                updateUser();
                XtraMessageBox.Show("تمت التعديل بنجاح", "تم", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
                return;
            }
        }
    }
}