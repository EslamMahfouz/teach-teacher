﻿namespace Teach.PL
{
    partial class frmEditUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditUser));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.cmbUsers = new DevExpress.XtraEditors.LookUpEdit();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataLayoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.AddStudentCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TransferStudentCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AddGroupCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.EditGroupCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AbsenceAndPaidCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReportsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AddUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.EditUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAddStudent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransferStudent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEditUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEditGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceAndPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReports = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.valName = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.valPhone = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.cmbUsers.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl2)).BeginInit();
            this.dataLayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddStudentCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferStudentCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddGroupCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditGroupCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceAndPaidCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceAndPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valPhone)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbUsers
            // 
            this.cmbUsers.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.valPhone.SetIconAlignment(this.cmbUsers, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            this.valName.SetIconAlignment(this.cmbUsers, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            this.cmbUsers.Location = new System.Drawing.Point(185, 13);
            this.cmbUsers.Name = "cmbUsers";
            this.cmbUsers.Properties.ActionButtonIndex = 1;
            this.cmbUsers.Properties.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.cmbUsers.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUsers.Properties.Appearance.Options.UseFont = true;
            this.cmbUsers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("cmbUsers.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUsers.Properties.NullText = "بحث";
            this.cmbUsers.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbUsers.Size = new System.Drawing.Size(927, 40);
            this.cmbUsers.TabIndex = 0;
            this.cmbUsers.EditValueChanged += new System.EventHandler(this.cmbUsers_EditValueChanged);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TelephoneTextEdit);
            this.dataLayoutControl1.Location = new System.Drawing.Point(185, 59);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(930, 217);
            this.dataLayoutControl1.TabIndex = 1;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.EnterMoveNextControl = true;
            this.NameTextEdit.Location = new System.Drawing.Point(31, 71);
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.NameTextEdit.Size = new System.Drawing.Size(791, 34);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 0;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "برجاء كتابة الإسم";
            this.valName.SetValidationRule(this.NameTextEdit, conditionValidationRule1);
            // 
            // AddressTextEdit
            // 
            this.AddressTextEdit.EnterMoveNextControl = true;
            this.AddressTextEdit.Location = new System.Drawing.Point(31, 111);
            this.AddressTextEdit.Name = "AddressTextEdit";
            this.AddressTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddressTextEdit.Properties.Appearance.Options.UseFont = true;
            this.AddressTextEdit.Size = new System.Drawing.Size(791, 34);
            this.AddressTextEdit.StyleController = this.dataLayoutControl1;
            this.AddressTextEdit.TabIndex = 1;
            // 
            // PhoneTextEdit
            // 
            this.PhoneTextEdit.EnterMoveNextControl = true;
            this.PhoneTextEdit.Location = new System.Drawing.Point(467, 151);
            this.PhoneTextEdit.Name = "PhoneTextEdit";
            this.PhoneTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PhoneTextEdit.Properties.Appearance.Options.UseFont = true;
            this.PhoneTextEdit.Size = new System.Drawing.Size(355, 34);
            this.PhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.PhoneTextEdit.TabIndex = 2;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "برجاء إدخال رقم المحمول";
            this.valPhone.SetValidationRule(this.PhoneTextEdit, conditionValidationRule2);
            // 
            // TelephoneTextEdit
            // 
            this.TelephoneTextEdit.EnterMoveNextControl = true;
            this.TelephoneTextEdit.Location = new System.Drawing.Point(31, 151);
            this.TelephoneTextEdit.Name = "TelephoneTextEdit";
            this.TelephoneTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TelephoneTextEdit.Properties.Appearance.Options.UseFont = true;
            this.TelephoneTextEdit.Size = new System.Drawing.Size(353, 34);
            this.TelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.TelephoneTextEdit.TabIndex = 3;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(930, 217);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup2.Size = new System.Drawing.Size(904, 191);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup3.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPhone,
            this.ItemForAddress,
            this.ItemForName,
            this.ItemForTelephone});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup3.Size = new System.Drawing.Size(904, 191);
            this.layoutControlGroup3.Text = "بيانات أساسية";
            // 
            // ItemForPhone
            // 
            this.ItemForPhone.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForPhone.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPhone.Control = this.PhoneTextEdit;
            this.ItemForPhone.Location = new System.Drawing.Point(436, 80);
            this.ItemForPhone.Name = "ItemForPhone";
            this.ItemForPhone.Size = new System.Drawing.Size(438, 41);
            this.ItemForPhone.Text = "المحمول";
            this.ItemForPhone.TextSize = new System.Drawing.Size(73, 29);
            // 
            // ItemForAddress
            // 
            this.ItemForAddress.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAddress.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAddress.Control = this.AddressTextEdit;
            this.ItemForAddress.Location = new System.Drawing.Point(0, 40);
            this.ItemForAddress.Name = "ItemForAddress";
            this.ItemForAddress.Size = new System.Drawing.Size(874, 40);
            this.ItemForAddress.Text = "العنوان";
            this.ItemForAddress.TextSize = new System.Drawing.Size(73, 29);
            // 
            // ItemForName
            // 
            this.ItemForName.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForName.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 0);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(874, 40);
            this.ItemForName.Text = "الإسم";
            this.ItemForName.TextSize = new System.Drawing.Size(73, 29);
            // 
            // ItemForTelephone
            // 
            this.ItemForTelephone.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForTelephone.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForTelephone.Control = this.TelephoneTextEdit;
            this.ItemForTelephone.Location = new System.Drawing.Point(0, 80);
            this.ItemForTelephone.Name = "ItemForTelephone";
            this.ItemForTelephone.Size = new System.Drawing.Size(436, 41);
            this.ItemForTelephone.Text = "التليفون";
            this.ItemForTelephone.TextSize = new System.Drawing.Size(73, 29);
            // 
            // dataLayoutControl2
            // 
            this.dataLayoutControl2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataLayoutControl2.Controls.Add(this.AddStudentCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.TransferStudentCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.AddGroupCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.EditGroupCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.AbsenceAndPaidCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.ReportsCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.AddUserCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.EditUserCheckEdit);
            this.dataLayoutControl2.Location = new System.Drawing.Point(138, 269);
            this.dataLayoutControl2.Name = "dataLayoutControl2";
            this.dataLayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(952, 188, 312, 437);
            this.dataLayoutControl2.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataLayoutControl2.Root = this.layoutControlGroup5;
            this.dataLayoutControl2.Size = new System.Drawing.Size(1035, 166);
            this.dataLayoutControl2.TabIndex = 3;
            this.dataLayoutControl2.Text = "dataLayoutControl2";
            // 
            // AddStudentCheckEdit
            // 
            this.AddStudentCheckEdit.Location = new System.Drawing.Point(766, 71);
            this.AddStudentCheckEdit.Name = "AddStudentCheckEdit";
            this.AddStudentCheckEdit.Properties.Caption = "Add Student";
            this.AddStudentCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AddStudentCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.AddStudentCheckEdit.StyleController = this.dataLayoutControl2;
            this.AddStudentCheckEdit.TabIndex = 4;
            // 
            // TransferStudentCheckEdit
            // 
            this.TransferStudentCheckEdit.Location = new System.Drawing.Point(522, 71);
            this.TransferStudentCheckEdit.Name = "TransferStudentCheckEdit";
            this.TransferStudentCheckEdit.Properties.Caption = "Transfer Student";
            this.TransferStudentCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TransferStudentCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.TransferStudentCheckEdit.StyleController = this.dataLayoutControl2;
            this.TransferStudentCheckEdit.TabIndex = 5;
            // 
            // AddGroupCheckEdit
            // 
            this.AddGroupCheckEdit.Location = new System.Drawing.Point(766, 106);
            this.AddGroupCheckEdit.Name = "AddGroupCheckEdit";
            this.AddGroupCheckEdit.Properties.Caption = "Add Group";
            this.AddGroupCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AddGroupCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.AddGroupCheckEdit.StyleController = this.dataLayoutControl2;
            this.AddGroupCheckEdit.TabIndex = 6;
            // 
            // EditGroupCheckEdit
            // 
            this.EditGroupCheckEdit.Location = new System.Drawing.Point(522, 106);
            this.EditGroupCheckEdit.Name = "EditGroupCheckEdit";
            this.EditGroupCheckEdit.Properties.Caption = "Edit Group";
            this.EditGroupCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EditGroupCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.EditGroupCheckEdit.StyleController = this.dataLayoutControl2;
            this.EditGroupCheckEdit.TabIndex = 7;
            // 
            // AbsenceAndPaidCheckEdit
            // 
            this.AbsenceAndPaidCheckEdit.Location = new System.Drawing.Point(275, 106);
            this.AbsenceAndPaidCheckEdit.Name = "AbsenceAndPaidCheckEdit";
            this.AbsenceAndPaidCheckEdit.Properties.Caption = "Absence And Paid";
            this.AbsenceAndPaidCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AbsenceAndPaidCheckEdit.Size = new System.Drawing.Size(22, 19);
            this.AbsenceAndPaidCheckEdit.StyleController = this.dataLayoutControl2;
            this.AbsenceAndPaidCheckEdit.TabIndex = 8;
            // 
            // ReportsCheckEdit
            // 
            this.ReportsCheckEdit.Location = new System.Drawing.Point(31, 106);
            this.ReportsCheckEdit.Name = "ReportsCheckEdit";
            this.ReportsCheckEdit.Properties.Caption = "Reports";
            this.ReportsCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReportsCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.ReportsCheckEdit.StyleController = this.dataLayoutControl2;
            this.ReportsCheckEdit.TabIndex = 9;
            // 
            // AddUserCheckEdit
            // 
            this.AddUserCheckEdit.Location = new System.Drawing.Point(275, 71);
            this.AddUserCheckEdit.Name = "AddUserCheckEdit";
            this.AddUserCheckEdit.Properties.Caption = "Add User";
            this.AddUserCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AddUserCheckEdit.Size = new System.Drawing.Size(22, 19);
            this.AddUserCheckEdit.StyleController = this.dataLayoutControl2;
            this.AddUserCheckEdit.TabIndex = 10;
            // 
            // EditUserCheckEdit
            // 
            this.EditUserCheckEdit.Location = new System.Drawing.Point(31, 71);
            this.EditUserCheckEdit.Name = "EditUserCheckEdit";
            this.EditUserCheckEdit.Properties.Caption = "Edit User";
            this.EditUserCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EditUserCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.EditUserCheckEdit.StyleController = this.dataLayoutControl2;
            this.EditUserCheckEdit.TabIndex = 11;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "Root";
            this.layoutControlGroup5.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup5.Size = new System.Drawing.Size(1035, 166);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AllowDrawBackground = false;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "autoGeneratedGroup0";
            this.layoutControlGroup6.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup6.Size = new System.Drawing.Size(1009, 140);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup7.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAddStudent,
            this.ItemForAddGroup,
            this.ItemForTransferStudent,
            this.ItemForAddUser,
            this.ItemForEditUser,
            this.ItemForEditGroup,
            this.ItemForAbsenceAndPaid,
            this.ItemForReports});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup7.Size = new System.Drawing.Size(1009, 140);
            this.layoutControlGroup7.Text = "الصلاحيات";
            // 
            // ItemForAddStudent
            // 
            this.ItemForAddStudent.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAddStudent.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAddStudent.Control = this.AddStudentCheckEdit;
            this.ItemForAddStudent.Location = new System.Drawing.Point(735, 0);
            this.ItemForAddStudent.Name = "ItemForAddStudent";
            this.ItemForAddStudent.Size = new System.Drawing.Size(244, 35);
            this.ItemForAddStudent.Text = "إضافة طالب";
            this.ItemForAddStudent.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForAddGroup
            // 
            this.ItemForAddGroup.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAddGroup.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAddGroup.Control = this.AddGroupCheckEdit;
            this.ItemForAddGroup.Location = new System.Drawing.Point(735, 35);
            this.ItemForAddGroup.Name = "ItemForAddGroup";
            this.ItemForAddGroup.Size = new System.Drawing.Size(244, 35);
            this.ItemForAddGroup.Text = "إضافة مجموعة";
            this.ItemForAddGroup.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForTransferStudent
            // 
            this.ItemForTransferStudent.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForTransferStudent.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForTransferStudent.Control = this.TransferStudentCheckEdit;
            this.ItemForTransferStudent.Location = new System.Drawing.Point(491, 0);
            this.ItemForTransferStudent.Name = "ItemForTransferStudent";
            this.ItemForTransferStudent.Size = new System.Drawing.Size(244, 35);
            this.ItemForTransferStudent.Text = "نقل وتعديل وحذف الطالب";
            this.ItemForTransferStudent.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForAddUser
            // 
            this.ItemForAddUser.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAddUser.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAddUser.Control = this.AddUserCheckEdit;
            this.ItemForAddUser.Location = new System.Drawing.Point(244, 0);
            this.ItemForAddUser.Name = "ItemForAddUser";
            this.ItemForAddUser.Size = new System.Drawing.Size(247, 35);
            this.ItemForAddUser.Text = "إضافة مستخدم";
            this.ItemForAddUser.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForEditUser
            // 
            this.ItemForEditUser.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForEditUser.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForEditUser.Control = this.EditUserCheckEdit;
            this.ItemForEditUser.Location = new System.Drawing.Point(0, 0);
            this.ItemForEditUser.Name = "ItemForEditUser";
            this.ItemForEditUser.Size = new System.Drawing.Size(244, 35);
            this.ItemForEditUser.Text = "تعديل مستخدم";
            this.ItemForEditUser.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForEditGroup
            // 
            this.ItemForEditGroup.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForEditGroup.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForEditGroup.Control = this.EditGroupCheckEdit;
            this.ItemForEditGroup.Location = new System.Drawing.Point(491, 35);
            this.ItemForEditGroup.Name = "ItemForEditGroup";
            this.ItemForEditGroup.Size = new System.Drawing.Size(244, 35);
            this.ItemForEditGroup.Text = "تعديل مجموعة";
            this.ItemForEditGroup.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForAbsenceAndPaid
            // 
            this.ItemForAbsenceAndPaid.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAbsenceAndPaid.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAbsenceAndPaid.Control = this.AbsenceAndPaidCheckEdit;
            this.ItemForAbsenceAndPaid.Location = new System.Drawing.Point(244, 35);
            this.ItemForAbsenceAndPaid.Name = "ItemForAbsenceAndPaid";
            this.ItemForAbsenceAndPaid.Size = new System.Drawing.Size(247, 35);
            this.ItemForAbsenceAndPaid.Text = "الدفع والغياب";
            this.ItemForAbsenceAndPaid.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForReports
            // 
            this.ItemForReports.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForReports.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForReports.Control = this.ReportsCheckEdit;
            this.ItemForReports.Location = new System.Drawing.Point(0, 35);
            this.ItemForReports.Name = "ItemForReports";
            this.ItemForReports.Size = new System.Drawing.Size(244, 35);
            this.ItemForReports.Text = "التقارير";
            this.ItemForReports.TextSize = new System.Drawing.Size(215, 29);
            // 
            // btnSave
            // 
            this.btnSave.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Appearance.Font = new System.Drawing.Font("Jozoor Font", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Enabled = false;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnSave.Location = new System.Drawing.Point(517, 441);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(330, 68);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmEditUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1311, 518);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.dataLayoutControl2);
            this.Controls.Add(this.cmbUsers);
            this.Name = "frmEditUser";
            this.Text = "تعديل مستخدم";
            this.Load += new System.EventHandler(this.frmEditUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmbUsers.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl2)).EndInit();
            this.dataLayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AddStudentCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferStudentCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddGroupCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditGroupCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceAndPaidCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceAndPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valPhone)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit cmbUsers;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.TextEdit AddressTextEdit;
        private DevExpress.XtraEditors.TextEdit PhoneTextEdit;
        private DevExpress.XtraEditors.TextEdit TelephoneTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTelephone;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl2;
        private DevExpress.XtraEditors.CheckEdit AddStudentCheckEdit;
        private DevExpress.XtraEditors.CheckEdit TransferStudentCheckEdit;
        private DevExpress.XtraEditors.CheckEdit AddGroupCheckEdit;
        private DevExpress.XtraEditors.CheckEdit EditGroupCheckEdit;
        private DevExpress.XtraEditors.CheckEdit AbsenceAndPaidCheckEdit;
        private DevExpress.XtraEditors.CheckEdit ReportsCheckEdit;
        private DevExpress.XtraEditors.CheckEdit AddUserCheckEdit;
        private DevExpress.XtraEditors.CheckEdit EditUserCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddStudent;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddGroup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferStudent;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddUser;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEditUser;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEditGroup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceAndPaid;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReports;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider valName;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider valPhone;
    }
}