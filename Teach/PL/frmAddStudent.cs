﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Teach.EDM;

namespace Teach.PL
{
    public partial class frmAddStudent : XtraForm
    {
        TeachEntities db = new TeachEntities();

        public int groupID, studentID, numInLog;
        public bool edit = false;
        private Boolean isExist(string nameStudent)
        {
            var student = (from x in db.Students
                           where x.nameStudent  == nameStudent
                           select x).ToList();
            if (student.Count == 0)
                return false;
            else
                return true;
        }

        void fillNew()
        {
            int num = Convert.ToInt32((from x in db.Students
             where x.idGroup == groupID
             orderby x.numInLog descending
             select x.numInLog).FirstOrDefault());
            numInLogTextEdit.Text =  (++num).ToString();

            var group = db.Groups.Find(groupID);
            PriceTextEdit.Text = group.price.ToString();
        }
        void fillOld()
        {
            var student = db.Students.Find(studentID);
            numInLogTextEdit.Text = student.numInLog.ToString();
            nameStudentTextEdit.Text = student.nameStudent;
            addressStudentTextEdit.Text = student.addressStudent;
            numberParentTextEdit.Text = student.numberParent;
            schoolStudentTextEdit.Text = student.schoolStudent;
            notesTextEdit.Text = student.notes;
            switch (student.Type)
            {
                case "عادي":
                    ExemptCheckEdit.SelectedIndex = 0;
                    break;
                case "مستثني":
                    ExemptCheckEdit.SelectedIndex = 1;
                    break;
                case "معفي":
                    ExemptCheckEdit.SelectedIndex = 2;
                    break;
            }
            PriceTextEdit.Text = student.Price.ToString();
            btnAdd.Text = "حفظ";
        }
        void newOne()
        {
            fillNew();
            nameStudentTextEdit.Text = "";
            addressStudentTextEdit.Text = "";
            schoolStudentTextEdit.Text = "";
            numberParentTextEdit.Text = "";
            notesTextEdit.Text = "";
            ExemptCheckEdit.SelectedIndex = 0;
            nameStudentTextEdit.Focus();
        }
        void update()
        {
            var student = db.Students.Find(studentID);
            student.nameStudent = nameStudentTextEdit.Text;
            student.addressStudent = addressStudentTextEdit.Text;
            student.numberParent = numberParentTextEdit.Text;
            student.schoolStudent = schoolStudentTextEdit.Text;
            student.notes = notesTextEdit.Text;

            switch (ExemptCheckEdit.SelectedIndex)
            {
                case 0:
                     student.Type = "عادي";
                    break;
                case 1:
                    student.Type = "مستثني";
                    break;
                case 2:
                    student.Type = "معفي";
                    break;
            }
            student.Price = Convert.ToDouble(PriceTextEdit.Text);
            db.SaveChanges();
            XtraMessageBox.Show("تم حفظ التعديلات", "تم", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }
        public frmAddStudent()
        {
            InitializeComponent();
            ActiveControl = nameStudentTextEdit;
        }

        private void frmAddStudent_Load(object sender, EventArgs e)
        {
            if (edit)
            {
                fillOld();
            }
            else
            {
                fillNew();
            }
        }

        private void ExemptCheckEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ExemptCheckEdit.SelectedIndex == 0)
            {
                PriceTextEdit.ReadOnly = true;
                var group = db.Groups.Find(groupID);
                PriceTextEdit.Text = group.price.ToString();
            }
            else if (ExemptCheckEdit.SelectedIndex == 1)
            {
                PriceTextEdit.ReadOnly = false;
            }
            else
            {
                PriceTextEdit.ReadOnly = true;
                PriceTextEdit.Text = "0";
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!valName.Validate())
            {
                nameStudentTextEdit.Focus();
                return;
            }
            if (isExist(nameStudentTextEdit.Text) && !edit)
            {
                XtraMessageBox.Show("يوجد طالب بهذا الإسم", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (edit)
            {
                update();
            }
            else
            {
                Student s = new Student()
                {
                    idGroup = groupID,
                    nameStudent = nameStudentTextEdit.Text,
                    addressStudent = addressStudentTextEdit.Text,
                    schoolStudent = schoolStudentTextEdit.Text,
                    numberParent = numberParentTextEdit.Text,
                    notes = notesTextEdit.Text,
                    numInLog = Convert.ToInt32(numInLogTextEdit.Text),
                    Price = Convert.ToDouble(PriceTextEdit.Text),
                    Type = ExemptCheckEdit.Text,
                };
                db.Students.Add(s);
                db.SaveChanges();
                XtraMessageBox.Show("تم إضافة الطالب بنجاح", "تم", MessageBoxButtons.OK, MessageBoxIcon.Information);
                newOne();
            }
        }
    }
}