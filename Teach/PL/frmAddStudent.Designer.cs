﻿namespace Teach.PL
{
    partial class frmAddStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.nameStudentTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.addressStudentTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.schoolStudentTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.numberParentTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.numInLogTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PriceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.notesTextEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ExemptCheckEdit = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemFornameStudent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForaddressStudent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForschoolStudent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFornumberParent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFornotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFornumInLog = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForExempt = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.valName = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameStudentTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressStudentTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schoolStudentTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberParentTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numInLogTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notesTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExemptCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornameStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForaddressStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForschoolStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornumberParent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornumInLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valName)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.nameStudentTextEdit);
            this.dataLayoutControl1.Controls.Add(this.addressStudentTextEdit);
            this.dataLayoutControl1.Controls.Add(this.schoolStudentTextEdit);
            this.dataLayoutControl1.Controls.Add(this.numberParentTextEdit);
            this.dataLayoutControl1.Controls.Add(this.numInLogTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PriceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.notesTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ExemptCheckEdit);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(601, 540);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // nameStudentTextEdit
            // 
            this.nameStudentTextEdit.Location = new System.Drawing.Point(31, 111);
            this.nameStudentTextEdit.Name = "nameStudentTextEdit";
            this.nameStudentTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameStudentTextEdit.Properties.Appearance.Options.UseFont = true;
            this.nameStudentTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.nameStudentTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameStudentTextEdit.Size = new System.Drawing.Size(430, 34);
            this.nameStudentTextEdit.StyleController = this.dataLayoutControl1;
            this.nameStudentTextEdit.TabIndex = 4;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "برجاء كتابة إسم الطالب";
            this.valName.SetValidationRule(this.nameStudentTextEdit, conditionValidationRule2);
            // 
            // addressStudentTextEdit
            // 
            this.addressStudentTextEdit.Location = new System.Drawing.Point(31, 151);
            this.addressStudentTextEdit.Name = "addressStudentTextEdit";
            this.addressStudentTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addressStudentTextEdit.Properties.Appearance.Options.UseFont = true;
            this.addressStudentTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.addressStudentTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.addressStudentTextEdit.Size = new System.Drawing.Size(430, 34);
            this.addressStudentTextEdit.StyleController = this.dataLayoutControl1;
            this.addressStudentTextEdit.TabIndex = 5;
            // 
            // schoolStudentTextEdit
            // 
            this.schoolStudentTextEdit.Location = new System.Drawing.Point(31, 191);
            this.schoolStudentTextEdit.Name = "schoolStudentTextEdit";
            this.schoolStudentTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schoolStudentTextEdit.Properties.Appearance.Options.UseFont = true;
            this.schoolStudentTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.schoolStudentTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.schoolStudentTextEdit.Size = new System.Drawing.Size(430, 34);
            this.schoolStudentTextEdit.StyleController = this.dataLayoutControl1;
            this.schoolStudentTextEdit.TabIndex = 6;
            // 
            // numberParentTextEdit
            // 
            this.numberParentTextEdit.Location = new System.Drawing.Point(31, 231);
            this.numberParentTextEdit.Name = "numberParentTextEdit";
            this.numberParentTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberParentTextEdit.Properties.Appearance.Options.UseFont = true;
            this.numberParentTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.numberParentTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.numberParentTextEdit.Size = new System.Drawing.Size(430, 34);
            this.numberParentTextEdit.StyleController = this.dataLayoutControl1;
            this.numberParentTextEdit.TabIndex = 7;
            // 
            // numInLogTextEdit
            // 
            this.numInLogTextEdit.Location = new System.Drawing.Point(31, 71);
            this.numInLogTextEdit.Name = "numInLogTextEdit";
            this.numInLogTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numInLogTextEdit.Properties.Appearance.Options.UseFont = true;
            this.numInLogTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.numInLogTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.numInLogTextEdit.Properties.Mask.EditMask = "N0";
            this.numInLogTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.numInLogTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.numInLogTextEdit.Properties.ReadOnly = true;
            this.numInLogTextEdit.Size = new System.Drawing.Size(430, 34);
            this.numInLogTextEdit.StyleController = this.dataLayoutControl1;
            this.numInLogTextEdit.TabIndex = 9;
            // 
            // PriceTextEdit
            // 
            this.PriceTextEdit.EditValue = 0D;
            this.PriceTextEdit.Location = new System.Drawing.Point(31, 475);
            this.PriceTextEdit.Name = "PriceTextEdit";
            this.PriceTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PriceTextEdit.Properties.Appearance.Options.UseFont = true;
            this.PriceTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PriceTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PriceTextEdit.Properties.Mask.EditMask = "f";
            this.PriceTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.PriceTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PriceTextEdit.Properties.ReadOnly = true;
            this.PriceTextEdit.Size = new System.Drawing.Size(430, 34);
            this.PriceTextEdit.StyleController = this.dataLayoutControl1;
            this.PriceTextEdit.TabIndex = 12;
            // 
            // notesTextEdit
            // 
            this.notesTextEdit.Location = new System.Drawing.Point(31, 271);
            this.notesTextEdit.Name = "notesTextEdit";
            this.notesTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notesTextEdit.Properties.Appearance.Options.UseFont = true;
            this.notesTextEdit.Size = new System.Drawing.Size(430, 85);
            this.notesTextEdit.StyleController = this.dataLayoutControl1;
            this.notesTextEdit.TabIndex = 8;
            // 
            // ExemptCheckEdit
            // 
            this.ExemptCheckEdit.EditValue = "عادي";
            this.ExemptCheckEdit.Location = new System.Drawing.Point(31, 432);
            this.ExemptCheckEdit.Name = "ExemptCheckEdit";
            this.ExemptCheckEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExemptCheckEdit.Properties.Appearance.Options.UseFont = true;
            this.ExemptCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ExemptCheckEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("عادي", "عادي"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("مستثني", "مستثني"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("معفي", "معفي")});
            this.ExemptCheckEdit.Size = new System.Drawing.Size(430, 37);
            this.ExemptCheckEdit.StyleController = this.dataLayoutControl1;
            this.ExemptCheckEdit.TabIndex = 10;
            this.ExemptCheckEdit.SelectedIndexChanged += new System.EventHandler(this.ExemptCheckEdit_SelectedIndexChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(601, 540);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup2.Size = new System.Drawing.Size(575, 514);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup3.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemFornameStudent,
            this.ItemForaddressStudent,
            this.ItemForschoolStudent,
            this.ItemFornumberParent,
            this.ItemFornotes,
            this.ItemFornumInLog});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup3.Size = new System.Drawing.Size(575, 361);
            this.layoutControlGroup3.Text = "بيانات عامة";
            // 
            // ItemFornameStudent
            // 
            this.ItemFornameStudent.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemFornameStudent.AppearanceItemCaption.Options.UseFont = true;
            this.ItemFornameStudent.Control = this.nameStudentTextEdit;
            this.ItemFornameStudent.Location = new System.Drawing.Point(0, 40);
            this.ItemFornameStudent.Name = "ItemFornameStudent";
            this.ItemFornameStudent.Size = new System.Drawing.Size(545, 40);
            this.ItemFornameStudent.Text = "إسم الطالب";
            this.ItemFornameStudent.TextSize = new System.Drawing.Size(105, 29);
            // 
            // ItemForaddressStudent
            // 
            this.ItemForaddressStudent.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForaddressStudent.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForaddressStudent.Control = this.addressStudentTextEdit;
            this.ItemForaddressStudent.Location = new System.Drawing.Point(0, 80);
            this.ItemForaddressStudent.Name = "ItemForaddressStudent";
            this.ItemForaddressStudent.Size = new System.Drawing.Size(545, 40);
            this.ItemForaddressStudent.Text = "العنوان";
            this.ItemForaddressStudent.TextSize = new System.Drawing.Size(105, 29);
            // 
            // ItemForschoolStudent
            // 
            this.ItemForschoolStudent.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForschoolStudent.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForschoolStudent.Control = this.schoolStudentTextEdit;
            this.ItemForschoolStudent.Location = new System.Drawing.Point(0, 120);
            this.ItemForschoolStudent.Name = "ItemForschoolStudent";
            this.ItemForschoolStudent.Size = new System.Drawing.Size(545, 40);
            this.ItemForschoolStudent.Text = "المدرسة";
            this.ItemForschoolStudent.TextSize = new System.Drawing.Size(105, 29);
            // 
            // ItemFornumberParent
            // 
            this.ItemFornumberParent.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemFornumberParent.AppearanceItemCaption.Options.UseFont = true;
            this.ItemFornumberParent.Control = this.numberParentTextEdit;
            this.ItemFornumberParent.Location = new System.Drawing.Point(0, 160);
            this.ItemFornumberParent.Name = "ItemFornumberParent";
            this.ItemFornumberParent.Size = new System.Drawing.Size(545, 40);
            this.ItemFornumberParent.Text = "رقم ولي الأمر";
            this.ItemFornumberParent.TextSize = new System.Drawing.Size(105, 29);
            // 
            // ItemFornotes
            // 
            this.ItemFornotes.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemFornotes.AppearanceItemCaption.Options.UseFont = true;
            this.ItemFornotes.Control = this.notesTextEdit;
            this.ItemFornotes.Location = new System.Drawing.Point(0, 200);
            this.ItemFornotes.Name = "ItemFornotes";
            this.ItemFornotes.Size = new System.Drawing.Size(545, 91);
            this.ItemFornotes.Text = "ملاحظات";
            this.ItemFornotes.TextSize = new System.Drawing.Size(105, 29);
            // 
            // ItemFornumInLog
            // 
            this.ItemFornumInLog.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemFornumInLog.AppearanceItemCaption.Options.UseFont = true;
            this.ItemFornumInLog.Control = this.numInLogTextEdit;
            this.ItemFornumInLog.Location = new System.Drawing.Point(0, 0);
            this.ItemFornumInLog.Name = "ItemFornumInLog";
            this.ItemFornumInLog.Size = new System.Drawing.Size(545, 40);
            this.ItemFornumInLog.Text = "رقم الكشف";
            this.ItemFornumInLog.TextSize = new System.Drawing.Size(105, 29);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup4.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForExempt,
            this.ItemForPrice});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 361);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup4.Size = new System.Drawing.Size(575, 153);
            this.layoutControlGroup4.Text = "بيانات الدفع";
            // 
            // ItemForExempt
            // 
            this.ItemForExempt.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForExempt.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForExempt.Control = this.ExemptCheckEdit;
            this.ItemForExempt.Location = new System.Drawing.Point(0, 0);
            this.ItemForExempt.Name = "ItemForExempt";
            this.ItemForExempt.Size = new System.Drawing.Size(545, 43);
            this.ItemForExempt.Text = "الحالة";
            this.ItemForExempt.TextSize = new System.Drawing.Size(105, 29);
            // 
            // ItemForPrice
            // 
            this.ItemForPrice.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForPrice.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPrice.Control = this.PriceTextEdit;
            this.ItemForPrice.Location = new System.Drawing.Point(0, 43);
            this.ItemForPrice.Name = "ItemForPrice";
            this.ItemForPrice.Size = new System.Drawing.Size(545, 40);
            this.ItemForPrice.Text = "الثمن";
            this.ItemForPrice.TextSize = new System.Drawing.Size(105, 29);
            // 
            // btnAdd
            // 
            this.btnAdd.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnAdd.Appearance.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Appearance.Options.UseFont = true;
            this.btnAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAdd.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAdd.Location = new System.Drawing.Point(31, 546);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(430, 73);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "إضافة";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // frmAddStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 631);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dataLayoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAddStudent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "إضافة طالب";
            this.Load += new System.EventHandler(this.frmAddStudent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nameStudentTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressStudentTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schoolStudentTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberParentTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numInLogTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notesTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExemptCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornameStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForaddressStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForschoolStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornumberParent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornumInLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit nameStudentTextEdit;
        private DevExpress.XtraEditors.TextEdit addressStudentTextEdit;
        private DevExpress.XtraEditors.TextEdit schoolStudentTextEdit;
        private DevExpress.XtraEditors.TextEdit numberParentTextEdit;
        private DevExpress.XtraEditors.TextEdit numInLogTextEdit;
        private DevExpress.XtraEditors.TextEdit PriceTextEdit;
        private DevExpress.XtraEditors.MemoEdit notesTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemFornameStudent;
        private DevExpress.XtraLayout.LayoutControlItem ItemForaddressStudent;
        private DevExpress.XtraLayout.LayoutControlItem ItemForschoolStudent;
        private DevExpress.XtraLayout.LayoutControlItem ItemFornumberParent;
        private DevExpress.XtraLayout.LayoutControlItem ItemFornotes;
        private DevExpress.XtraLayout.LayoutControlItem ItemFornumInLog;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExempt;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrice;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.RadioGroup ExemptCheckEdit;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider valName;
    }
}