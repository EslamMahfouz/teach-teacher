﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Teach.EDM;

namespace Teach.PL
{
    public partial class frmAddUser : XtraForm
    {
        TeachEntities db = new TeachEntities();        
        public frmAddUser()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!valName.Validate())
                {
                    NameTextEdit.Focus();
                    return;
                }
                if (!valPhone.Validate())
                {
                    PhoneTextEdit.Focus();
                    return;
                }
                if (!valUsername.Validate())
                {
                    userNameTextEdit.Focus();
                    return;
                }
                if (!valPassword.Validate())
                {
                    passwordTextEdit.Focus();
                    return;
                }
                if (!valConfirm.Validate())
                {
                    ConfirmTextEdit.Focus();
                    return;
                }

                var user = (from x in db.Users
                            where x.userName == userNameTextEdit.Text
                            select x).ToList();
                if (user.Count > 0)
                {
                    XtraMessageBox.Show(" إسم الدخول مستخدم بالفعل", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    userNameTextEdit.Focus();
                    userNameTextEdit.SelectionStart = 0;
                    userNameTextEdit.SelectionLength = userNameTextEdit.Text.Length;
                    return;
                }

                User u = new User()
                {
                    Name = NameTextEdit.Text,
                    Address = AddressTextEdit.Text,
                    Phone = PhoneTextEdit.Text,
                    Telephone = TelephoneTextEdit.Text,
                    userName = userNameTextEdit.Text,
                    password = passwordTextEdit.Text
                };
                db.Users.Add(u);

                UserAccess ua = new UserAccess()
                {
                    UserID = u.UserID,
                    AddStudent = Convert.ToBoolean(AddStudentCheckEdit.EditValue),
                    TransferStudent = Convert.ToBoolean(TransferStudentCheckEdit.EditValue),
                    AddGroup = Convert.ToBoolean(AddGroupCheckEdit.EditValue),
                    EditGroup = Convert.ToBoolean(EditGroupCheckEdit.EditValue),
                    AbsenceAndPaid = Convert.ToBoolean(AbsenceAndPaidCheckEdit.EditValue),
                    Reports = Convert.ToBoolean(ReportsCheckEdit.EditValue),
                    AddUser = Convert.ToBoolean(AddUserCheckEdit.EditValue),
                    EditUser = Convert.ToBoolean(EditUserCheckEdit.EditValue)
                };
                db.UserAccesses.Add(ua);

                db.SaveChanges();
                XtraMessageBox.Show("تمت الإضافة بنجاح", "تم", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
                return;
            }
        }
    }
}