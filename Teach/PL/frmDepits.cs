﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraEditors;
using Teach.EDM;
using DevExpress.XtraEditors.Repository;
using System.Drawing;

namespace Teach.PL
{
    public partial class frmDepits : XtraForm
    {
        TeachEntities db = new TeachEntities();

        public frmDepits()
        {
            InitializeComponent();
        }

        private void frmDepits_Load(object sender, EventArgs e)
        {
            RepositoryItemDateEdit riDateEdit = new RepositoryItemDateEdit();
            gridControl1.RepositoryItems.Add(riDateEdit);

            var std = (from x in db.Absences
                       where x.Carry > 0
                       orderby x.date ascending
                       select new
                       {
                           الإسم = x.Student.nameStudent,
                           المجموعة = x.Student.Group.nameGroup,
                           رقم_ولي_الأمر = x.Student.numberParent,
                           الشهر = x.date,
                           المتبقي = x.Carry
                       }).ToList();
            gridControl1.DataSource = std;
            gridView1.PopulateColumns();
            gridView1.Columns["الشهر"].ColumnEdit = riDateEdit;
            riDateEdit.EditMask = "MM/yyyy";
            riDateEdit.Mask.UseMaskAsDisplayFormat = true;
            gridView1.Columns["الشهر"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;

            gridView1.Columns["الإسم"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
            gridView1.Columns["المجموعة"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
            gridView1.Columns["رقم_ولي_الأمر"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
            gridView1.Columns["الشهر"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
            gridView1.Columns["المتبقي"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);

            gridView1.Columns["الإسم"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
            gridView1.Columns["المجموعة"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
            gridView1.Columns["رقم_ولي_الأمر"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
            gridView1.Columns["الشهر"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
            gridView1.Columns["المتبقي"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);

            gridView1.Columns["الإسم"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المجموعة"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["رقم_ولي_الأمر"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["الشهر"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المتبقي"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            gridView1.Columns["الإسم"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المجموعة"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["رقم_ولي_الأمر"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["الشهر"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المتبقي"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            gridView1.Columns["المتبقي"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "المتبقي", "الإجمالي ={0:n2}");

        }

        private void gridView1_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
                e.Info.Kind = DevExpress.Utils.Drawing.IndicatorKind.Row;
            }
        }
    }
}