﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Teach.EDM;
using DevExpress.XtraReports.UI;

namespace Teach.PL
{
    public partial class frmPay : XtraForm
    {
        TeachEntities db = new TeachEntities();

        public int idStudent, year, month;
        double carry;
        string name = "", group = "";
        private void txtPaid_EditValueChanged(object sender, EventArgs e)
        {
            txtCarry.Text = (carry - Convert.ToDouble(txtPaid.Text)).ToString();
            if (Convert.ToDouble(txtCarry.Text) < 0)
            {
                txtPaid.Text = "0";
                txtCarry.Text = carry.ToString();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(txtPaid.Text) > 0)
            {
                var absence = from x in db.Absences
                              where x.date.Year == year && x.date.Month == month && x.idStudent == idStudent
                              select x;

                foreach (var item in absence)
                {
                    item.Paid += Convert.ToDouble(txtPaid.Text);
                    item.Carry = Convert.ToDouble(txtCarry.Text);
                    break;
                }
                Income i = new Income()
                {
                    Date = DateTime.Now,
                    StudentName = name,
                    GroupName = group,
                    Price = txtPaid.Text,
                    Receipt = Program.username
                };
                db.Incomes.Add(i);
                db.SaveChanges();
                XtraMessageBox.Show("تم الدفع", "تم", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (XtraMessageBox.Show("طباعة إيصال؟", "سؤال", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Reports.repPaid report = new Reports.repPaid();
                    report.parameter1.Value = name;
                    report.parameter2.Value = DateTime.Now;
                    report.paramPrice.Value = txtPaid.Text;
                    report.ShowPreview();
                }
            }
            Close();
        }

        public frmPay()
        {
            InitializeComponent();
        }

        private void frmPay_Load(object sender, EventArgs e)
        {
            try
            {
                var absence = from x in db.Absences
                              where x.date.Year == year && x.date.Month == month && x.idStudent == idStudent
                              select x;

                foreach (var item in absence)
                {
                    name = item.Student.nameStudent;
                    group = item.Student.Group.nameGroup; 
                    carry = Convert.ToDouble(item.Carry);
                    txtCarry.Text = carry.ToString();
                }
            }
            catch
            {
                return;
            }
        }
    }
}