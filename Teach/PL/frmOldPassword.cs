﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Teach.EDM;

namespace Teach.PL
{
    public partial class frmOldPassword : XtraForm
    {
        TeachEntities db = new TeachEntities();

        public frmOldPassword()
        {
            InitializeComponent();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            var user = from x in db.Users
                       where x.userName == Program.username && x.password == txtPassword.Text
                       select x;

            if (user.ToList().Count > 0)
            {
                frmChangePassword frm = new frmChangePassword();
                foreach (var item in user)
                {
                    frm.userID = item.UserID;
                    break;
                }
                frm.ShowDialog();
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("برجاء التأكد أن الرقم صحيح", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtPassword.Focus();
                txtPassword.SelectionStart = 0;
                txtPassword.SelectionLength = txtPassword.Text.Length;
            }
        }
    }
}