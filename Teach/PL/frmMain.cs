﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTabbedMdi;
using DevExpress.XtraEditors;
using System.IO;
using System.Data.SqlClient;
using Teach.EDM;
using System.Globalization;
using DevExpress.Utils.Animation;
using System.Drawing;

namespace Teach.PL
{
    public partial class frmMain : XtraForm
    {
        TeachEntities db = new TeachEntities();

        void addForm(XtraForm frm)
        {
            showCharts(false);

            List<Form> openForms = new List<Form>();
            foreach (Form f in Application.OpenForms)
                openForms.Add(f);

            foreach (Form f in openForms)
            {
                if (f.Name != "frmMain")
                    f.Close();
            }

            frm.MdiParent = this;
            frm.Show();
        }
        void fillCharts()
        {
            try
            {
                string day = DateTime.Now.ToString("dddd", new CultureInfo("ar-EG"));
                var gps = (from x in db.Relations
                           where x.Day == day
                           select new { م = x.idGroup, المجموعة = x.Group.nameGroup, الساعة = x.Time }).ToList();
                gridControl1.DataSource = gps;
                cardView1.PopulateColumns();
                cardView1.Columns["م"].Visible = false;
            }
            catch
            {
                return;
            }
        }
        void showCharts(bool status)
        {
            groupControl1.Visible = status;
            gridControl1.Visible = status;
        }
       
        public frmMain()
        {
            InitializeComponent();
        }
        private void frmMain_Load(object sender, EventArgs e)
        { 
            if (Properties.Settings.Default.isPaid == false)
            {
                var _frm = new frmActivate();
                _frm.ShowDialog();
            }

            var xfrm = new frmLogin();
            xfrm.ShowDialog();
            barUser.Caption = Program.username;
            fillCharts();
            showCharts(true);
        }

        private void btnPrintingSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmPrintingSettings frm = new frmPrintingSettings();
            frm.ShowDialog();
        }
        private void btnCenterData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCenterData frm = new frmCenterData();
            addForm(frm);
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                SqlConnection sqlconnection = new SqlConnection(@"Server=.\SQLEXPRESS; Database=master; Integrated Security=true");
                SqlCommand cmd;

                string combined = Path.Combine(Properties.Settings.Default.BackUpFolder, "TeachBackup.bak");
                File.Delete(combined);
                string query = "Backup Database Teach to Disk='" + combined + "'";
                cmd = new SqlCommand(query, sqlconnection);
                sqlconnection.Open();
                cmd.ExecuteNonQuery();
                sqlconnection.Close();
            }
            catch
            {
                return;
            }
        }

        private void xtraTabbedMdiManager1_SelectedPageChanged(object sender, EventArgs e)
        {
            showCharts(false);
        }

        private void xtraTabbedMdiManager1_PageRemoved(object sender, MdiTabPageEventArgs e)
        {
            showCharts(true);
            fillCharts();
        }

        private void btnAddGroup_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.addGroup)
            {
                var frm = new frmAddGroup();
                addForm(frm);
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
       }

        private void btnShowGroups_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmGroups frm = new frmGroups();
            addForm(frm);
        }

        private void btnChangeGroup_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.transferStudent)
            {
                var frm = new frmChangeGroup();
                frm.ShowDialog();
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnDepits_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmDepits frm = new frmDepits();
            addForm(frm);
        }

        private void btnAddUser_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.addUser)
            {
                frmAddUser frm = new frmAddUser();
                addForm(frm);
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnEditUser_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.editUser)
            {
                frmEditUser frm = new frmEditUser();
                addForm(frm);
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnIncome_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.reports)
            {
                frmIncome frm = new frmIncome();
                addForm(frm);
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnChangePassword_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmOldPassword frm = new frmOldPassword();
            frm.ShowDialog();
        }

        private void btnLogout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmLogin frm = new frmLogin();
            frm.ShowDialog();
            barUser.Caption = Program.username;
        }

        private void cardView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                frmGroups frm = new frmGroups();
                frm.cmbGroup.EditValue = Convert.ToInt32(cardView1.GetFocusedRowCellValue("م"));
                addForm(frm);
            }
            catch
            {
                return;
            }
        }
    }
}


