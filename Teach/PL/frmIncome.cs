﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Drawing;
using System.Linq;

namespace Teach.PL
{
    public partial class frmIncome : XtraForm
    {
        EDM.TeachEntities db = new EDM.TeachEntities();

        public frmIncome()
        {
            InitializeComponent();
            DateTime today = DateTime.Now.Date;
            deFrom.EditValue = today;
            deTo.EditValue = today;
        }

        private void frmIncome_Load(object sender, EventArgs e)
        {
            DateTime dateFrom = Convert.ToDateTime(deFrom.EditValue);
            DateTime dateTo = Convert.ToDateTime(deTo.EditValue);


            var incomes = from i in db.Incomes
                     where ((EntityFunctions.TruncateTime(i.Date)) >= dateFrom && (EntityFunctions.TruncateTime(i.Date)) <= dateTo)
                     orderby i.cashID ascending
                     select new
                     {
                         الطالب = i.StudentName,
                         المجموعة = i.GroupName,
                         المدفوع = i.Price,
                         التاريخ = i.Date,
                         المستلم = i.Receipt
                     };
            gridControl1.DataSource = incomes.ToList();

            gridView1.PopulateColumns();
            gridView1.BestFitColumns();

            gridView1.Columns["الطالب"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
            gridView1.Columns["المجموعة"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
            gridView1.Columns["المدفوع"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
            gridView1.Columns["التاريخ"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
            gridView1.Columns["المستلم"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);

            gridView1.Columns["الطالب"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
            gridView1.Columns["المجموعة"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
            gridView1.Columns["المدفوع"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
            gridView1.Columns["التاريخ"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
            gridView1.Columns["المستلم"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);

            gridView1.Columns["الطالب"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المجموعة"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المدفوع"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["التاريخ"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المستلم"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            gridView1.Columns["الطالب"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المجموعة"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المدفوع"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["التاريخ"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المستلم"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            gridView1.Columns["المدفوع"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "المدفوع", "الإجمالي ={0:n2}");
        }

        private void gridView1_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
                e.Info.Kind = DevExpress.Utils.Drawing.IndicatorKind.Row;
            }
        }

        private void deTo_EditValueChanged(object sender, EventArgs e)
        {
            DateTime dateFrom = Convert.ToDateTime(deFrom.EditValue);
            DateTime dateTo = Convert.ToDateTime(deTo.EditValue);

            var incomes = from i in db.Incomes
                          where ((EntityFunctions.TruncateTime(i.Date)) >= dateFrom && (EntityFunctions.TruncateTime(i.Date)) <= dateTo)
                          orderby i.cashID ascending
                          select new
                          {
                              الطالب = i.StudentName,
                              المجموعة = i.GroupName,
                              المدفوع = i.Price,
                              التاريخ = i.Date,
                              المستلم = i.Receipt
                          };
            gridControl1.DataSource = incomes.ToList();
        }
    }
}
