﻿namespace Teach.PL
{
    partial class frmAddUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule4 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule compareAgainstControlValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddUser));
            this.passwordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.ConfirmTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.userNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForpassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForuserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.valName = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.valPhone = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.valUsername = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.valPassword = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.valConfirm = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.dataLayoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.AddStudentCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TransferStudentCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AddGroupCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.EditGroupCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AbsenceAndPaidCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReportsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AddUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.EditUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAddStudent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransferStudent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEditUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEditGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceAndPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReports = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.passwordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForpassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForuserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valUsername)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl2)).BeginInit();
            this.dataLayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddStudentCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferStudentCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddGroupCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditGroupCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceAndPaidCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceAndPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReports)).BeginInit();
            this.SuspendLayout();
            // 
            // passwordTextEdit
            // 
            this.passwordTextEdit.EnterMoveNextControl = true;
            this.passwordTextEdit.Location = new System.Drawing.Point(385, 301);
            this.passwordTextEdit.Name = "passwordTextEdit";
            this.passwordTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordTextEdit.Properties.Appearance.Options.UseFont = true;
            this.passwordTextEdit.Properties.PasswordChar = '*';
            this.passwordTextEdit.Size = new System.Drawing.Size(202, 34);
            this.passwordTextEdit.StyleController = this.dataLayoutControl1;
            this.passwordTextEdit.TabIndex = 5;
            conditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule4.ErrorText = "برجاء كتابة الرقم السري";
            this.valPassword.SetValidationRule(this.passwordTextEdit, conditionValidationRule4);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataLayoutControl1.Controls.Add(this.ConfirmTextEdit);
            this.dataLayoutControl1.Controls.Add(this.userNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.passwordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Location = new System.Drawing.Point(253, 17);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(766, 370);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ConfirmTextEdit
            // 
            this.ConfirmTextEdit.EnterMoveNextControl = true;
            this.ConfirmTextEdit.Location = new System.Drawing.Point(31, 301);
            this.ConfirmTextEdit.Name = "ConfirmTextEdit";
            this.ConfirmTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfirmTextEdit.Properties.Appearance.Options.UseFont = true;
            this.ConfirmTextEdit.Properties.PasswordChar = '*';
            this.ConfirmTextEdit.Size = new System.Drawing.Size(200, 34);
            this.ConfirmTextEdit.StyleController = this.dataLayoutControl1;
            this.ConfirmTextEdit.TabIndex = 6;
            compareAgainstControlValidationRule1.CompareControlOperator = DevExpress.XtraEditors.DXErrorProvider.CompareControlOperator.Equals;
            compareAgainstControlValidationRule1.Control = this.passwordTextEdit;
            compareAgainstControlValidationRule1.ErrorText = "الرقم السري غير متطابق";
            this.valConfirm.SetValidationRule(this.ConfirmTextEdit, compareAgainstControlValidationRule1);
            // 
            // userNameTextEdit
            // 
            this.userNameTextEdit.EnterMoveNextControl = true;
            this.userNameTextEdit.Location = new System.Drawing.Point(31, 261);
            this.userNameTextEdit.Name = "userNameTextEdit";
            this.userNameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userNameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.userNameTextEdit.Size = new System.Drawing.Size(556, 34);
            this.userNameTextEdit.StyleController = this.dataLayoutControl1;
            this.userNameTextEdit.TabIndex = 4;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "برجاء كتابة إسم الدخول";
            this.valUsername.SetValidationRule(this.userNameTextEdit, conditionValidationRule1);
            // 
            // AddressTextEdit
            // 
            this.AddressTextEdit.EnterMoveNextControl = true;
            this.AddressTextEdit.Location = new System.Drawing.Point(31, 151);
            this.AddressTextEdit.Name = "AddressTextEdit";
            this.AddressTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddressTextEdit.Properties.Appearance.Options.UseFont = true;
            this.AddressTextEdit.Size = new System.Drawing.Size(556, 34);
            this.AddressTextEdit.StyleController = this.dataLayoutControl1;
            this.AddressTextEdit.TabIndex = 1;
            // 
            // PhoneTextEdit
            // 
            this.PhoneTextEdit.EnterMoveNextControl = true;
            this.PhoneTextEdit.Location = new System.Drawing.Point(385, 111);
            this.PhoneTextEdit.Name = "PhoneTextEdit";
            this.PhoneTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PhoneTextEdit.Properties.Appearance.Options.UseFont = true;
            this.PhoneTextEdit.Size = new System.Drawing.Size(202, 34);
            this.PhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.PhoneTextEdit.TabIndex = 2;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "برجاء إدخال رقم المحمول";
            this.valPhone.SetValidationRule(this.PhoneTextEdit, conditionValidationRule2);
            // 
            // TelephoneTextEdit
            // 
            this.TelephoneTextEdit.EnterMoveNextControl = true;
            this.TelephoneTextEdit.Location = new System.Drawing.Point(31, 111);
            this.TelephoneTextEdit.Name = "TelephoneTextEdit";
            this.TelephoneTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TelephoneTextEdit.Properties.Appearance.Options.UseFont = true;
            this.TelephoneTextEdit.Size = new System.Drawing.Size(200, 34);
            this.TelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.TelephoneTextEdit.TabIndex = 3;
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.EnterMoveNextControl = true;
            this.NameTextEdit.Location = new System.Drawing.Point(31, 71);
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.NameTextEdit.Size = new System.Drawing.Size(556, 34);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 0;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "برجاء كتابة الإسم";
            this.valName.SetValidationRule(this.NameTextEdit, conditionValidationRule3);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(766, 370);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup2.Size = new System.Drawing.Size(740, 344);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup3.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPhone,
            this.ItemForAddress,
            this.ItemForName,
            this.ItemForTelephone});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup3.Size = new System.Drawing.Size(740, 190);
            this.layoutControlGroup3.Text = "بيانات أساسية";
            // 
            // ItemForPhone
            // 
            this.ItemForPhone.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForPhone.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPhone.Control = this.PhoneTextEdit;
            this.ItemForPhone.Location = new System.Drawing.Point(354, 40);
            this.ItemForPhone.Name = "ItemForPhone";
            this.ItemForPhone.Size = new System.Drawing.Size(356, 40);
            this.ItemForPhone.Text = "المحمول";
            this.ItemForPhone.TextSize = new System.Drawing.Size(144, 29);
            // 
            // ItemForAddress
            // 
            this.ItemForAddress.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAddress.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAddress.Control = this.AddressTextEdit;
            this.ItemForAddress.Location = new System.Drawing.Point(0, 80);
            this.ItemForAddress.Name = "ItemForAddress";
            this.ItemForAddress.Size = new System.Drawing.Size(710, 40);
            this.ItemForAddress.Text = "العنوان";
            this.ItemForAddress.TextSize = new System.Drawing.Size(144, 29);
            // 
            // ItemForName
            // 
            this.ItemForName.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForName.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 0);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(710, 40);
            this.ItemForName.Text = "الإسم";
            this.ItemForName.TextSize = new System.Drawing.Size(144, 29);
            // 
            // ItemForTelephone
            // 
            this.ItemForTelephone.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForTelephone.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForTelephone.Control = this.TelephoneTextEdit;
            this.ItemForTelephone.Location = new System.Drawing.Point(0, 40);
            this.ItemForTelephone.Name = "ItemForTelephone";
            this.ItemForTelephone.Size = new System.Drawing.Size(354, 40);
            this.ItemForTelephone.Text = "التليفون";
            this.ItemForTelephone.TextSize = new System.Drawing.Size(144, 29);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup4.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForpassword,
            this.ItemForuserName,
            this.layoutControlItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 190);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup4.Size = new System.Drawing.Size(740, 154);
            this.layoutControlGroup4.Text = "بيانات الدخول";
            // 
            // ItemForpassword
            // 
            this.ItemForpassword.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForpassword.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForpassword.Control = this.passwordTextEdit;
            this.ItemForpassword.Location = new System.Drawing.Point(354, 40);
            this.ItemForpassword.Name = "ItemForpassword";
            this.ItemForpassword.Size = new System.Drawing.Size(356, 44);
            this.ItemForpassword.Text = "الرقم السري";
            this.ItemForpassword.TextSize = new System.Drawing.Size(144, 29);
            // 
            // ItemForuserName
            // 
            this.ItemForuserName.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForuserName.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForuserName.Control = this.userNameTextEdit;
            this.ItemForuserName.Location = new System.Drawing.Point(0, 0);
            this.ItemForuserName.Name = "ItemForuserName";
            this.ItemForuserName.Size = new System.Drawing.Size(710, 40);
            this.ItemForuserName.Text = "إسم الدخول";
            this.ItemForuserName.TextSize = new System.Drawing.Size(144, 29);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.ConfirmTextEdit;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(354, 44);
            this.layoutControlItem1.Text = "تأكيد الرقم السري";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(144, 29);
            // 
            // btnAdd
            // 
            this.btnAdd.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAdd.Appearance.Font = new System.Drawing.Font("Jozoor Font", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Appearance.Options.UseFont = true;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAdd.Location = new System.Drawing.Point(455, 550);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(330, 63);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "إضافة";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dataLayoutControl2
            // 
            this.dataLayoutControl2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataLayoutControl2.Controls.Add(this.AddStudentCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.TransferStudentCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.AddGroupCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.EditGroupCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.AbsenceAndPaidCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.ReportsCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.AddUserCheckEdit);
            this.dataLayoutControl2.Controls.Add(this.EditUserCheckEdit);
            this.dataLayoutControl2.Location = new System.Drawing.Point(103, 373);
            this.dataLayoutControl2.Name = "dataLayoutControl2";
            this.dataLayoutControl2.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataLayoutControl2.Root = this.layoutControlGroup5;
            this.dataLayoutControl2.Size = new System.Drawing.Size(1034, 171);
            this.dataLayoutControl2.TabIndex = 2;
            this.dataLayoutControl2.Text = "dataLayoutControl2";
            // 
            // AddStudentCheckEdit
            // 
            this.AddStudentCheckEdit.Location = new System.Drawing.Point(765, 71);
            this.AddStudentCheckEdit.Name = "AddStudentCheckEdit";
            this.AddStudentCheckEdit.Properties.Caption = "Add Student";
            this.AddStudentCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AddStudentCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.AddStudentCheckEdit.StyleController = this.dataLayoutControl2;
            this.AddStudentCheckEdit.TabIndex = 4;
            // 
            // TransferStudentCheckEdit
            // 
            this.TransferStudentCheckEdit.Location = new System.Drawing.Point(521, 71);
            this.TransferStudentCheckEdit.Name = "TransferStudentCheckEdit";
            this.TransferStudentCheckEdit.Properties.Caption = "Transfer Student";
            this.TransferStudentCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TransferStudentCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.TransferStudentCheckEdit.StyleController = this.dataLayoutControl2;
            this.TransferStudentCheckEdit.TabIndex = 5;
            // 
            // AddGroupCheckEdit
            // 
            this.AddGroupCheckEdit.Location = new System.Drawing.Point(765, 106);
            this.AddGroupCheckEdit.Name = "AddGroupCheckEdit";
            this.AddGroupCheckEdit.Properties.Caption = "Add Group";
            this.AddGroupCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AddGroupCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.AddGroupCheckEdit.StyleController = this.dataLayoutControl2;
            this.AddGroupCheckEdit.TabIndex = 6;
            // 
            // EditGroupCheckEdit
            // 
            this.EditGroupCheckEdit.Location = new System.Drawing.Point(521, 106);
            this.EditGroupCheckEdit.Name = "EditGroupCheckEdit";
            this.EditGroupCheckEdit.Properties.Caption = "Edit Group";
            this.EditGroupCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EditGroupCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.EditGroupCheckEdit.StyleController = this.dataLayoutControl2;
            this.EditGroupCheckEdit.TabIndex = 7;
            // 
            // AbsenceAndPaidCheckEdit
            // 
            this.AbsenceAndPaidCheckEdit.Location = new System.Drawing.Point(275, 106);
            this.AbsenceAndPaidCheckEdit.Name = "AbsenceAndPaidCheckEdit";
            this.AbsenceAndPaidCheckEdit.Properties.Caption = "Absence And Paid";
            this.AbsenceAndPaidCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AbsenceAndPaidCheckEdit.Size = new System.Drawing.Size(21, 19);
            this.AbsenceAndPaidCheckEdit.StyleController = this.dataLayoutControl2;
            this.AbsenceAndPaidCheckEdit.TabIndex = 8;
            // 
            // ReportsCheckEdit
            // 
            this.ReportsCheckEdit.Location = new System.Drawing.Point(31, 106);
            this.ReportsCheckEdit.Name = "ReportsCheckEdit";
            this.ReportsCheckEdit.Properties.Caption = "Reports";
            this.ReportsCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReportsCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.ReportsCheckEdit.StyleController = this.dataLayoutControl2;
            this.ReportsCheckEdit.TabIndex = 9;
            // 
            // AddUserCheckEdit
            // 
            this.AddUserCheckEdit.Location = new System.Drawing.Point(275, 71);
            this.AddUserCheckEdit.Name = "AddUserCheckEdit";
            this.AddUserCheckEdit.Properties.Caption = "Add User";
            this.AddUserCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AddUserCheckEdit.Size = new System.Drawing.Size(21, 19);
            this.AddUserCheckEdit.StyleController = this.dataLayoutControl2;
            this.AddUserCheckEdit.TabIndex = 10;
            // 
            // EditUserCheckEdit
            // 
            this.EditUserCheckEdit.Location = new System.Drawing.Point(31, 71);
            this.EditUserCheckEdit.Name = "EditUserCheckEdit";
            this.EditUserCheckEdit.Properties.Caption = "Edit User";
            this.EditUserCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EditUserCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.EditUserCheckEdit.StyleController = this.dataLayoutControl2;
            this.EditUserCheckEdit.TabIndex = 11;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "Root";
            this.layoutControlGroup5.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup5.Size = new System.Drawing.Size(1034, 171);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AllowDrawBackground = false;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "autoGeneratedGroup0";
            this.layoutControlGroup6.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup6.Size = new System.Drawing.Size(1008, 145);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup7.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAddStudent,
            this.ItemForAddGroup,
            this.ItemForTransferStudent,
            this.ItemForAddUser,
            this.ItemForEditUser,
            this.ItemForEditGroup,
            this.ItemForAbsenceAndPaid,
            this.ItemForReports});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup7.Size = new System.Drawing.Size(1008, 145);
            this.layoutControlGroup7.Text = "الصلاحيات";
            // 
            // ItemForAddStudent
            // 
            this.ItemForAddStudent.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAddStudent.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAddStudent.Control = this.AddStudentCheckEdit;
            this.ItemForAddStudent.Location = new System.Drawing.Point(734, 0);
            this.ItemForAddStudent.Name = "ItemForAddStudent";
            this.ItemForAddStudent.Size = new System.Drawing.Size(244, 35);
            this.ItemForAddStudent.Text = "إضافة طالب";
            this.ItemForAddStudent.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForAddGroup
            // 
            this.ItemForAddGroup.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAddGroup.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAddGroup.Control = this.AddGroupCheckEdit;
            this.ItemForAddGroup.Location = new System.Drawing.Point(734, 35);
            this.ItemForAddGroup.Name = "ItemForAddGroup";
            this.ItemForAddGroup.Size = new System.Drawing.Size(244, 40);
            this.ItemForAddGroup.Text = "إضافة مجموعة";
            this.ItemForAddGroup.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForTransferStudent
            // 
            this.ItemForTransferStudent.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForTransferStudent.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForTransferStudent.Control = this.TransferStudentCheckEdit;
            this.ItemForTransferStudent.Location = new System.Drawing.Point(490, 0);
            this.ItemForTransferStudent.Name = "ItemForTransferStudent";
            this.ItemForTransferStudent.Size = new System.Drawing.Size(244, 35);
            this.ItemForTransferStudent.Text = "نقل وتعديل وحذف الطالب";
            this.ItemForTransferStudent.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForAddUser
            // 
            this.ItemForAddUser.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAddUser.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAddUser.Control = this.AddUserCheckEdit;
            this.ItemForAddUser.Location = new System.Drawing.Point(244, 0);
            this.ItemForAddUser.Name = "ItemForAddUser";
            this.ItemForAddUser.Size = new System.Drawing.Size(246, 35);
            this.ItemForAddUser.Text = "إضافة مستخدم";
            this.ItemForAddUser.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForEditUser
            // 
            this.ItemForEditUser.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForEditUser.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForEditUser.Control = this.EditUserCheckEdit;
            this.ItemForEditUser.Location = new System.Drawing.Point(0, 0);
            this.ItemForEditUser.Name = "ItemForEditUser";
            this.ItemForEditUser.Size = new System.Drawing.Size(244, 35);
            this.ItemForEditUser.Text = "تعديل مستخدم";
            this.ItemForEditUser.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForEditGroup
            // 
            this.ItemForEditGroup.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForEditGroup.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForEditGroup.Control = this.EditGroupCheckEdit;
            this.ItemForEditGroup.Location = new System.Drawing.Point(490, 35);
            this.ItemForEditGroup.Name = "ItemForEditGroup";
            this.ItemForEditGroup.Size = new System.Drawing.Size(244, 40);
            this.ItemForEditGroup.Text = "تعديل مجموعة";
            this.ItemForEditGroup.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForAbsenceAndPaid
            // 
            this.ItemForAbsenceAndPaid.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForAbsenceAndPaid.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAbsenceAndPaid.Control = this.AbsenceAndPaidCheckEdit;
            this.ItemForAbsenceAndPaid.Location = new System.Drawing.Point(244, 35);
            this.ItemForAbsenceAndPaid.Name = "ItemForAbsenceAndPaid";
            this.ItemForAbsenceAndPaid.Size = new System.Drawing.Size(246, 40);
            this.ItemForAbsenceAndPaid.Text = "الدفع والغياب";
            this.ItemForAbsenceAndPaid.TextSize = new System.Drawing.Size(215, 29);
            // 
            // ItemForReports
            // 
            this.ItemForReports.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForReports.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForReports.Control = this.ReportsCheckEdit;
            this.ItemForReports.Location = new System.Drawing.Point(0, 35);
            this.ItemForReports.Name = "ItemForReports";
            this.ItemForReports.Size = new System.Drawing.Size(244, 40);
            this.ItemForReports.Text = "التقارير";
            this.ItemForReports.TextSize = new System.Drawing.Size(215, 29);
            // 
            // frmAddUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 623);
            this.Controls.Add(this.dataLayoutControl2);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "frmAddUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "إضافة مستخدم";
            ((System.ComponentModel.ISupportInitialize)(this.passwordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForpassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForuserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valUsername)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl2)).EndInit();
            this.dataLayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AddStudentCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferStudentCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddGroupCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditGroupCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceAndPaidCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceAndPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReports)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit userNameTextEdit;
        private DevExpress.XtraEditors.TextEdit passwordTextEdit;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.TextEdit AddressTextEdit;
        private DevExpress.XtraEditors.TextEdit PhoneTextEdit;
        private DevExpress.XtraEditors.TextEdit TelephoneTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForuserName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForpassword;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTelephone;
        private DevExpress.XtraEditors.TextEdit ConfirmTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider valUsername;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider valPassword;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider valName;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider valPhone;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider valConfirm;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.CheckEdit AddStudentCheckEdit;
        private DevExpress.XtraEditors.CheckEdit TransferStudentCheckEdit;
        private DevExpress.XtraEditors.CheckEdit AddGroupCheckEdit;
        private DevExpress.XtraEditors.CheckEdit EditGroupCheckEdit;
        private DevExpress.XtraEditors.CheckEdit AbsenceAndPaidCheckEdit;
        private DevExpress.XtraEditors.CheckEdit ReportsCheckEdit;
        private DevExpress.XtraEditors.CheckEdit AddUserCheckEdit;
        private DevExpress.XtraEditors.CheckEdit EditUserCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddStudent;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddGroup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferStudent;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddUser;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEditUser;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEditGroup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceAndPaid;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReports;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
    }
}