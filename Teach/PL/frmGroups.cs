﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Teach.BL;
using Teach.Reports;
using DevExpress.XtraReports.UI;
using Teach.EDM;
using System.Globalization;
using System.Drawing;
using System.IO;

namespace Teach.PL
{
    public partial class frmGroups : XtraForm
    {
        TeachEntities db = new TeachEntities();
        clsFill f = new clsFill(); clsGet g = new clsGet(); clsAdd a = new clsAdd();
        clsAbsence absence = new clsAbsence();

        public int idGroup;
        bool firstTime = true;
        private void fillAbsence()
        {
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;

            var stds = from x in db.Students
                       where x.idGroup == idGroup
                      select x;

            foreach (var item in stds)
            {
                var abs = from x in db.Absences
                          where x.idStudent == item.idStudent && x.date.Year == year && x.date.Month == month
                          select x;
                if (abs.ToList().Count == 0)
                {
                    a.addAbsence(item.idStudent);
                }
            }
        }
        private void fillGrids()
        {
            try
            {
                var group = db.Groups.Find(idGroup);
                nameGroupTextEdit.Text = group.nameGroup;
                priceTextEdit.Text = group.price.ToString();

                var date = Convert.ToDateTime(dateEdit1.EditValue);
                DateTime lastMonth = date.AddMonths(-1);

                var dates = from x in db.Relations
                            where x.idGroup == idGroup
                            select new { م = x.idRelation, اليوم = x.Day, الساعة = x.Time };
                gridControl1.DataSource = dates.ToList();
                gridView4.Columns["م"].Visible = false;

                gridView4.Columns["اليوم"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);
                gridView4.Columns["الساعة"].AppearanceHeader.Font = new Font("Calibri", 14, FontStyle.Bold);

                gridView4.Columns["اليوم"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);
                gridView4.Columns["الساعة"].AppearanceCell.Font = new Font("Calibri", 14, FontStyle.Regular);

                gridView4.Columns["اليوم"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                gridView4.Columns["الساعة"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                gridView4.Columns["اليوم"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                gridView4.Columns["الساعة"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                getStudentsOfGroupTableAdapter.Fill(dsGetStudentsofGroup.getStudentsOfGroup, idGroup);
                getAbsenceOfGroupTableAdapter.Fill(dsGetAbsenceOfGroup.getAbsenceOfGroup, idGroup, date);
                gridView3.BestFitColumns();
                gridView2.BestFitColumns();

                if (firstTime)
                {
                    gridView3.Columns["السعر"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "السعر", "الإجمالي ={0:n2}");
                    gridView3.Columns["مدفوع"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "مدفوع", "الإجمالي ={0:n2}");
                    gridView3.Columns["متبقي"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "متبقي", "الإجمالي ={0:n2}");
                    firstTime = false;
                }
            }
            catch 
            {
                return;
            }
        }
        
        public frmGroups()
        {
            InitializeComponent();
            dateEdit1.EditValue = DateTime.Now.Date;
        }
        private void frmGroups_Load(object sender, EventArgs e)
        {
            f.fillStages(cmbStage);
            cmbStage.EditValue = 1;
        }
        private void cmbStage_EditValueChanged(object sender, EventArgs e)
        {
            var idStage = Convert.ToInt32(cmbStage.EditValue);
            g.getClasses(idStage, cmbClass);
            cmbClass.EditValue = 3;
        }
        private void cmbClass_EditValueChanged(object sender, EventArgs e)
        {
            var idClass = Convert.ToInt32(cmbClass.EditValue);
            g.getGroups(idClass, cmbGroup);
        }
        public void cmbGroup_EditValueChanged(object sender, EventArgs e)
        {
            btnAddStudent.Enabled = true;
            btnAddDate.Enabled = true;
            btnDeleteDate.Enabled = true;
            btnDeleteGroup.Enabled = true;
            idGroup = Convert.ToInt32(cmbGroup.EditValue);
            fillAbsence();
            fillGrids();
        }

        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            fillAbsence();
            fillGrids();
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            fillAbsence();
            fillGrids();
        }
        private void btnAddDate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Program.editGroup)
                {
                    groupControl2.Visible = true;
                }
                else
                {
                    XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch
            {
                return;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var day = cmbDays.Text;
                var hour = teTime.Text;
                Relation r = new Relation()
                {
                    idGroup = idGroup,
                    Day = day,
                    Time = hour,
                };
                db.Relations.Add(r);
                db.SaveChanges();
                XtraMessageBox.Show("تم إضافة المعاد", "إضافة", MessageBoxButtons.OK, MessageBoxIcon.Information);
                groupControl2.Visible = false;
                cmbGroup_EditValueChanged(sender, e);
            }
            catch
            {
                XtraMessageBox.Show("برجاء ملء جميع الحقول", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            try
            {
                if (Program.editGroup)
                {
                    if (XtraMessageBox.Show("تأكيد حذف المجموعة؟", "حذف", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var group = db.Groups.Find(idGroup);
                        db.Groups.Remove(group);
                        db.SaveChanges();
                        XtraMessageBox.Show("تم الحذف بنجاح", "حذف", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        gridControl1.DataSource = null;
                        cmbClass_EditValueChanged(sender, e);
                        Close();
                    }
                }
                else
                {
                    XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch
            {
                return;
            }
        }
        private void btnDeleteDate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Program.editGroup)
                {
                    int idRelation = Convert.ToInt32(gridView4.GetFocusedRowCellValue("م"));
                    var relation = db.Relations.Find(idRelation);
                    db.Relations.Remove(relation);
                    db.SaveChanges();
                    cmbGroup_EditValueChanged(sender, e);
                }
                else
                {
                    XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch
            {
                return;
            }
        }

        private void gridView3_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                if (Program.paidAndAbsence)
                {
                    var idStudent = Convert.ToInt32(gridView3.GetFocusedRowCellValue(colم));
                    var date = Convert.ToDateTime(dateEdit1.EditValue);
                    int year = date.Year, month = date.Month;

                    var day1 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col1));
                    var day2 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col2));
                    var day3 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col3));
                    var day4 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col4));
                    var day5 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col5));
                    var day6 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col6));
                    var day7 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col7));
                    var day8 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col8));
                    var day9 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col9));
                    var day10 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col10));
                    var day11 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col11));
                    var day12 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col12));
                    var day13 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col13));
                    var day14 = Convert.ToBoolean(gridView3.GetFocusedRowCellValue(col14));
                    var exam = Convert.ToDouble(gridView3.GetFocusedRowCellValue(colالإمتحان));

                    var absence = from x in db.Absences
                                  where x.date.Year == year && x.date.Month == month && x.idStudent == idStudent
                                  select x;
                    foreach (var abs in absence)
                    {
                        abs.day1 = day1;
                        abs.day2 = day2;
                        abs.day3 = day3;
                        abs.day4 = day4;
                        abs.day5 = day5;
                        abs.day6 = day6;
                        abs.day7 = day7;
                        abs.day8 = day8;
                        abs.day9 = day9;
                        abs.day10 = day10;
                        abs.day11 = day11;
                        abs.day12 = day12;
                        abs.day13 = day13;
                        abs.day14 = day14;
                        abs.Exam = exam;
                        break;
                    }
                    db.SaveChanges();
                }
                else
                {
                    XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch
            {
                return;
            }
        }
        
        private void btnReport_Click(object sender, EventArgs e)
        {
            //try
            //{
                var report = new repMonthly();
                var attend = 0;

                var idStudent = Convert.ToInt32(gridView3.GetFocusedRowCellValue(colم));
                var date = Convert.ToDateTime(dateEdit1.EditValue);
                var cd = db.CenterDatas.Find(1);
                report.CenterName.Value = cd.Name;
                report.address.Value = cd.Address;
                report.Phone1.Value = cd.Phone1;
                report.Phone2.Value = cd.Phone2;
                report.subject.Value = cd.Subject;
                byte[] img = cd.Logo;
                MemoryStream ms = new MemoryStream(img);
                Image _img = Image.FromStream(ms);
                report.xrPictureBox1.Image = _img;

                report.month.Value = date.ToString("MMMM، yyyy", new CultureInfo("ar-EG"));
                var dt = absence.repMonthly(idStudent, date);

                report.paramDegree.Value = dt.Rows[0]["exam"].ToString();
                report.paramMaxDegree.Value = Properties.Settings.Default.maxDegree;
                report.paramName.Value = gridView3.GetFocusedRowCellValue(colالطالب).ToString();
            report.paramPaid.Value = dt.Rows[0]["paid"].ToString();
            report.paramCarry.Value = dt.Rows[0]["carry"].ToString();
            for (var i = 2; i < 16; i++)
                {
                    if (Convert.ToBoolean(dt.Rows[0][i]))
                    {
                        attend++;
                    }
                }
                report.paramAttend.Value = attend;
                report.paramTotal.Value = Properties.Settings.Default.totalAttendance;
                report.ShowPreview();
            //}
            //catch
            //{
            //    return;
            //}
        }
        private void btnRepGroup_Click(object sender, EventArgs e)
        {
            try
            {
                var report = new repMonthly();
                var date = Convert.ToDateTime(dateEdit1.EditValue);
                var cd = db.CenterDatas.Find(1);
                report.Name = cd.Name;
                report.address.Value = cd.Address;
                report.Phone1.Value = cd.Phone1;
                report.Phone2.Value = cd.Phone2;
                report.subject.Value = cd.Subject;
                DateTime month = Convert.ToDateTime(dateEdit1.EditValue);
                report.month.Value = month.ToString("MMMM، yyyy", new CultureInfo("ar-EG"));

                for (int i = 0; i < gridView3.RowCount; i++)
                {
                    var attend = 0;
                    var idStudent = Convert.ToInt32(gridView3.GetRowCellValue(i, colم));
                    var dt = absence.repMonthly(idStudent, date);
                    report.paramDegree.Value = dt.Rows[0]["exam"].ToString();
                    report.paramMaxDegree.Value = Properties.Settings.Default.maxDegree;
                    report.paramName.Value = gridView3.GetRowCellValue(i, colالطالب).ToString();
                    if (Convert.ToBoolean(dt.Rows[0]["paid"]))
                        report.paramPaid.Value = "تم الدفع";
                    else
                        report.paramPaid.Value = "لم يتم الدفع";
                    for (var x = 2; x < 16; x++)
                    {
                        if (Convert.ToBoolean(dt.Rows[0][x]))
                        {
                            attend++;
                        }
                    }
                    report.paramAttend.Value = attend;
                    report.paramTotal.Value = Properties.Settings.Default.totalAttendance;
                    report.CreateDocument();
                    report.Print(Properties.Settings.Default.Printer);
                }
            }
            catch
            {
                return;
            }
        }

        private void btnPrintGrid_Click(object sender, EventArgs e)
        {
            gridView3.ShowPrintPreview();
        }

        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            if(Program.addStudent)
            {
                frmAddStudent frm = new frmAddStudent();
                frm.groupID = idGroup;
                frm.ShowDialog();
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void repositoryItemButtonEdit2_Click(object sender, EventArgs e)
        {
            if (Program.transferStudent)
            {
                frmAddStudent frm = new frmAddStudent();
                frm.groupID = idGroup;
                frm.studentID = Convert.ToInt32(gridView2.GetFocusedRowCellValue("م"));
                frm.edit = true;
                frm.ShowDialog();
                fillGrids();
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void repositoryItemButtonEdit3_Click(object sender, EventArgs e)
        {
            if (Program.transferStudent)
            {
                if (XtraMessageBox.Show("تأكيد الحذف؟", "تنبيه", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    int studentID = Convert.ToInt32(gridView2.GetFocusedRowCellValue("م"));
                    var student = db.Students.Find(studentID);
                    db.Students.Remove(student);
                    db.SaveChanges();
                    fillGrids();
                }
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void frmGroups_Shown(object sender, EventArgs e)
        {
            gridView3.BestFitColumns();
            gridView2.BestFitColumns();
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            try
            {
                if (Program.editGroup)
                {
                    if (btnSaveChanges.Text == "تعديل")
                    {
                        nameGroupTextEdit.ReadOnly = false;
                        priceTextEdit.ReadOnly = false;
                        btnSaveChanges.Text = "حفظ";
                    }
                    else
                    {
                        var group = db.Groups.Find(idGroup);
                        group.nameGroup = nameGroupTextEdit.Text;
                        group.price = Convert.ToDouble(priceTextEdit.Text);
                        var students = from x in db.Students
                                       where x.idGroup == idGroup && x.Type == "عادي"
                                       select x;
                        foreach (var item in students)
                        {
                            item.Price = Convert.ToDouble(priceTextEdit.Text);
                        }

                        db.SaveChanges();
                        XtraMessageBox.Show("تم حفظ التعديلات", "تم", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        nameGroupTextEdit.ReadOnly = true;
                        priceTextEdit.ReadOnly = true;
                        btnSaveChanges.Text = "تعديل";
                    }
                }
                else
                {
                    XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch
            {
                return;
            }
        }

        private void repositoryItemButtonEdit5_Click(object sender, EventArgs e)
        {
            if (Program.paidAndAbsence)
            {
                int studentID = Convert.ToInt32(gridView3.GetFocusedRowCellValue("م"));
                var student = db.Students.Find(studentID);
                if (student.Type == "معفي")
                {
                    XtraMessageBox.Show("هذا الطالب معفي", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                frmPay frm = new frmPay();
                frm.idStudent = studentID;
                var date = Convert.ToDateTime(dateEdit1.EditValue);
                frm.year = date.Year;
                frm.month = date.Month;
                frm.ShowDialog();
                fillGrids();
            }
            else
            {
                XtraMessageBox.Show("غير مسموح لك", "تنبيه", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
