﻿namespace Teach.PL
{
    partial class frmGroups
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGroups));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colhour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnameGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnameClass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnameStage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnameTeacher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cmbStage = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbClass = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.btnPrintGrid = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.btnRepGroup = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnReport = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.getAbsenceOfGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsGetAbsenceOfGroup = new Teach.dsGetAbsenceOfGroup();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colم = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colرقمالكشف = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colالطالب = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colالشهر = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colالإمتحان = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colالسعر = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colمدفوع = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colمتبقي = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.getStudentsOfGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsGetStudentsofGroup = new Teach.dsGetStudentsofGroup();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colم1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colالإسم = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colرقمالكشف1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colالنوع = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colسعرالشهر = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnDeleteDate = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteGroup = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddDate = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.teTime = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDays = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.btnSaveChanges = new DevExpress.XtraEditors.SimpleButton();
            this.nameGroupTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.priceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemFornameGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForprice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnAddStudent = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.getAbsenceOfGroupTableAdapter = new Teach.dsGetAbsenceOfGroupTableAdapters.getAbsenceOfGroupTableAdapter();
            this.getStudentsOfGroupTableAdapter = new Teach.dsGetStudentsofGroupTableAdapters.getStudentsOfGroupTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroup.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getAbsenceOfGroupBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGetAbsenceOfGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getStudentsOfGroupBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGetStudentsofGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameGroupTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornameGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForprice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // colhour
            // 
            this.colhour.AppearanceCell.Options.UseTextOptions = true;
            this.colhour.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colhour.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colhour.AppearanceHeader.Options.UseFont = true;
            this.colhour.AppearanceHeader.Options.UseTextOptions = true;
            this.colhour.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colhour.Caption = "الساعة";
            this.colhour.FieldName = "hour";
            this.colhour.Name = "colhour";
            this.colhour.OptionsColumn.AllowEdit = false;
            // 
            // colday
            // 
            this.colday.AppearanceCell.Options.UseTextOptions = true;
            this.colday.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colday.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colday.AppearanceHeader.Options.UseFont = true;
            this.colday.AppearanceHeader.Options.UseTextOptions = true;
            this.colday.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colday.Caption = "اليوم";
            this.colday.FieldName = "day";
            this.colday.Name = "colday";
            this.colday.OptionsColumn.AllowEdit = false;
            this.colday.Width = 104;
            // 
            // colnameGroup
            // 
            this.colnameGroup.AppearanceCell.Options.UseTextOptions = true;
            this.colnameGroup.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnameGroup.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colnameGroup.AppearanceHeader.Options.UseFont = true;
            this.colnameGroup.AppearanceHeader.Options.UseTextOptions = true;
            this.colnameGroup.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnameGroup.Caption = "المجموعة";
            this.colnameGroup.FieldName = "nameGroup";
            this.colnameGroup.Name = "colnameGroup";
            this.colnameGroup.OptionsColumn.AllowEdit = false;
            this.colnameGroup.Width = 83;
            // 
            // colnameClass
            // 
            this.colnameClass.AppearanceCell.Options.UseTextOptions = true;
            this.colnameClass.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnameClass.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colnameClass.AppearanceHeader.Options.UseFont = true;
            this.colnameClass.AppearanceHeader.Options.UseTextOptions = true;
            this.colnameClass.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnameClass.Caption = "الصف";
            this.colnameClass.FieldName = "nameClass";
            this.colnameClass.Name = "colnameClass";
            this.colnameClass.OptionsColumn.AllowEdit = false;
            // 
            // colnameStage
            // 
            this.colnameStage.AppearanceCell.Options.UseTextOptions = true;
            this.colnameStage.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnameStage.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colnameStage.AppearanceHeader.Options.UseFont = true;
            this.colnameStage.AppearanceHeader.Options.UseTextOptions = true;
            this.colnameStage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnameStage.Caption = "المرحلة";
            this.colnameStage.FieldName = "nameStage";
            this.colnameStage.Name = "colnameStage";
            this.colnameStage.OptionsColumn.AllowEdit = false;
            // 
            // colnameTeacher
            // 
            this.colnameTeacher.AppearanceCell.Options.UseTextOptions = true;
            this.colnameTeacher.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnameTeacher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colnameTeacher.AppearanceHeader.Options.UseFont = true;
            this.colnameTeacher.AppearanceHeader.Options.UseTextOptions = true;
            this.colnameTeacher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnameTeacher.Caption = "المدرس";
            this.colnameTeacher.FieldName = "nameTeacher";
            this.colnameTeacher.Name = "colnameTeacher";
            this.colnameTeacher.OptionsColumn.AllowEdit = false;
            // 
            // cmbStage
            // 
            this.cmbStage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbStage.Location = new System.Drawing.Point(1194, 12);
            this.cmbStage.Name = "cmbStage";
            this.cmbStage.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStage.Properties.Appearance.Options.UseFont = true;
            this.cmbStage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStage.Properties.NullText = "إختر مرحلة";
            this.cmbStage.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbStage.Size = new System.Drawing.Size(146, 40);
            this.cmbStage.TabIndex = 6;
            this.cmbStage.EditValueChanged += new System.EventHandler(this.cmbStage_EditValueChanged);
            // 
            // cmbClass
            // 
            this.cmbClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbClass.Location = new System.Drawing.Point(1032, 12);
            this.cmbClass.Name = "cmbClass";
            this.cmbClass.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClass.Properties.Appearance.Options.UseFont = true;
            this.cmbClass.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbClass.Properties.NullText = "إختر صف";
            this.cmbClass.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbClass.Size = new System.Drawing.Size(156, 40);
            this.cmbClass.TabIndex = 7;
            this.cmbClass.EditValueChanged += new System.EventHandler(this.cmbClass_EditValueChanged);
            // 
            // cmbGroup
            // 
            this.cmbGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbGroup.Location = new System.Drawing.Point(774, 12);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGroup.Properties.Appearance.Options.UseFont = true;
            this.cmbGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbGroup.Properties.NullText = "إختر مجموعة";
            this.cmbGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbGroup.Size = new System.Drawing.Size(252, 40);
            this.cmbGroup.TabIndex = 8;
            this.cmbGroup.EditValueChanged += new System.EventHandler(this.cmbGroup_EditValueChanged);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.btnPrintGrid);
            this.xtraTabPage3.Controls.Add(this.dateEdit1);
            this.xtraTabPage3.Controls.Add(this.btnRepGroup);
            this.xtraTabPage3.Controls.Add(this.labelControl1);
            this.xtraTabPage3.Controls.Add(this.btnReport);
            this.xtraTabPage3.Controls.Add(this.groupControl4);
            this.xtraTabPage3.Controls.Add(this.gridControl3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1338, 551);
            this.xtraTabPage3.Text = "الغياب + الدفع";
            // 
            // btnPrintGrid
            // 
            this.btnPrintGrid.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnPrintGrid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPrintGrid.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnPrintGrid.Appearance.Options.UseFont = true;
            this.btnPrintGrid.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintGrid.Image")));
            this.btnPrintGrid.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnPrintGrid.Location = new System.Drawing.Point(200, 16);
            this.btnPrintGrid.Name = "btnPrintGrid";
            this.btnPrintGrid.Size = new System.Drawing.Size(208, 48);
            this.btnPrintGrid.TabIndex = 10;
            this.btnPrintGrid.Text = "طباعة";
            this.btnPrintGrid.Click += new System.EventHandler(this.btnPrintGrid_Click);
            // 
            // dateEdit1
            // 
            this.dateEdit1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(845, 26);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.Mask.EditMask = "MM/yyyy";
            this.dateEdit1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit1.Size = new System.Drawing.Size(195, 30);
            this.dateEdit1.TabIndex = 9;
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // btnRepGroup
            // 
            this.btnRepGroup.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnRepGroup.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRepGroup.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnRepGroup.Appearance.Options.UseFont = true;
            this.btnRepGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnRepGroup.Image")));
            this.btnRepGroup.Location = new System.Drawing.Point(414, 16);
            this.btnRepGroup.Name = "btnRepGroup";
            this.btnRepGroup.Size = new System.Drawing.Size(180, 48);
            this.btnRepGroup.TabIndex = 7;
            this.btnRepGroup.Text = "تقرير للمجموعة";
            this.btnRepGroup.Click += new System.EventHandler(this.btnRepGroup_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(1046, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(92, 24);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "بيانات شهر";
            // 
            // btnReport
            // 
            this.btnReport.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnReport.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnReport.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnReport.Appearance.Options.UseFont = true;
            this.btnReport.Image = ((System.Drawing.Image)(resources.GetObject("btnReport.Image")));
            this.btnReport.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnReport.Location = new System.Drawing.Point(600, 16);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(180, 48);
            this.btnReport.TabIndex = 3;
            this.btnReport.Text = "تقرير";
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl4.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl4.Controls.Add(this.simpleButton2);
            this.groupControl4.Controls.Add(this.simpleButton3);
            this.groupControl4.Controls.Add(this.simpleButton4);
            this.groupControl4.Enabled = false;
            this.groupControl4.Location = new System.Drawing.Point(1540, 286);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(146, 175);
            this.groupControl4.TabIndex = 1;
            this.groupControl4.Text = "عمليات متاحة";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Location = new System.Drawing.Point(6, 84);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(135, 40);
            this.simpleButton2.TabIndex = 4;
            this.simpleButton2.Text = "حذف معاد";
            this.simpleButton2.Click += new System.EventHandler(this.btnDeleteDate_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(6, 130);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(135, 40);
            this.simpleButton3.TabIndex = 3;
            this.simpleButton3.Text = "حذف المجموعة";
            this.simpleButton3.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Location = new System.Drawing.Point(6, 38);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(135, 40);
            this.simpleButton4.TabIndex = 2;
            this.simpleButton4.Text = "إضافة معاد";
            this.simpleButton4.Click += new System.EventHandler(this.btnAddDate_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.getAbsenceOfGroupBindingSource;
            this.gridControl3.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridControl3.Location = new System.Drawing.Point(6, 70);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit5});
            this.gridControl3.Size = new System.Drawing.Size(1328, 476);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3,
            this.gridView1});
            // 
            // getAbsenceOfGroupBindingSource
            // 
            this.getAbsenceOfGroupBindingSource.DataMember = "getAbsenceOfGroup";
            this.getAbsenceOfGroupBindingSource.DataSource = this.dsGetAbsenceOfGroup;
            // 
            // dsGetAbsenceOfGroup
            // 
            this.dsGetAbsenceOfGroup.DataSetName = "dsGetAbsenceOfGroup";
            this.dsGetAbsenceOfGroup.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView3.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.ColumnPanelRowHeight = 50;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colم,
            this.colرقمالكشف,
            this.colالطالب,
            this.colالشهر,
            this.col1,
            this.col2,
            this.col3,
            this.col4,
            this.col5,
            this.col6,
            this.col7,
            this.col8,
            this.col9,
            this.col10,
            this.col11,
            this.col12,
            this.col13,
            this.col14,
            this.colالإمتحان,
            this.colالسعر,
            this.colمدفوع,
            this.colمتبقي,
            this.gridColumn3});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.RowHeight = 50;
            this.gridView3.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView3_CellValueChanged);
            // 
            // colم
            // 
            this.colم.FieldName = "م";
            this.colم.Name = "colم";
            // 
            // colرقمالكشف
            // 
            this.colرقمالكشف.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colرقمالكشف.AppearanceCell.Options.UseFont = true;
            this.colرقمالكشف.AppearanceCell.Options.UseTextOptions = true;
            this.colرقمالكشف.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colرقمالكشف.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colرقمالكشف.AppearanceHeader.Options.UseFont = true;
            this.colرقمالكشف.AppearanceHeader.Options.UseTextOptions = true;
            this.colرقمالكشف.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colرقمالكشف.FieldName = "رقم الكشف";
            this.colرقمالكشف.Name = "colرقمالكشف";
            this.colرقمالكشف.OptionsColumn.AllowEdit = false;
            this.colرقمالكشف.Visible = true;
            this.colرقمالكشف.VisibleIndex = 0;
            // 
            // colالطالب
            // 
            this.colالطالب.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالطالب.AppearanceCell.Options.UseFont = true;
            this.colالطالب.AppearanceCell.Options.UseTextOptions = true;
            this.colالطالب.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالطالب.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالطالب.AppearanceHeader.Options.UseFont = true;
            this.colالطالب.AppearanceHeader.Options.UseTextOptions = true;
            this.colالطالب.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالطالب.FieldName = "الطالب";
            this.colالطالب.Name = "colالطالب";
            this.colالطالب.OptionsColumn.AllowEdit = false;
            this.colالطالب.Visible = true;
            this.colالطالب.VisibleIndex = 1;
            // 
            // colالشهر
            // 
            this.colالشهر.FieldName = "الشهر";
            this.colالشهر.Name = "colالشهر";
            // 
            // col1
            // 
            this.col1.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col1.AppearanceCell.Options.UseFont = true;
            this.col1.AppearanceCell.Options.UseTextOptions = true;
            this.col1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col1.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col1.AppearanceHeader.Options.UseFont = true;
            this.col1.AppearanceHeader.Options.UseTextOptions = true;
            this.col1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col1.FieldName = "1";
            this.col1.Name = "col1";
            this.col1.Visible = true;
            this.col1.VisibleIndex = 2;
            // 
            // col2
            // 
            this.col2.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col2.AppearanceCell.Options.UseFont = true;
            this.col2.AppearanceCell.Options.UseTextOptions = true;
            this.col2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col2.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col2.AppearanceHeader.Options.UseFont = true;
            this.col2.AppearanceHeader.Options.UseTextOptions = true;
            this.col2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col2.FieldName = "2";
            this.col2.Name = "col2";
            this.col2.Visible = true;
            this.col2.VisibleIndex = 3;
            // 
            // col3
            // 
            this.col3.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col3.AppearanceCell.Options.UseFont = true;
            this.col3.AppearanceCell.Options.UseTextOptions = true;
            this.col3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col3.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col3.AppearanceHeader.Options.UseFont = true;
            this.col3.AppearanceHeader.Options.UseTextOptions = true;
            this.col3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col3.FieldName = "3";
            this.col3.Name = "col3";
            this.col3.Visible = true;
            this.col3.VisibleIndex = 4;
            // 
            // col4
            // 
            this.col4.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col4.AppearanceCell.Options.UseFont = true;
            this.col4.AppearanceCell.Options.UseTextOptions = true;
            this.col4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col4.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col4.AppearanceHeader.Options.UseFont = true;
            this.col4.AppearanceHeader.Options.UseTextOptions = true;
            this.col4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col4.FieldName = "4";
            this.col4.Name = "col4";
            this.col4.Visible = true;
            this.col4.VisibleIndex = 5;
            // 
            // col5
            // 
            this.col5.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col5.AppearanceCell.Options.UseFont = true;
            this.col5.AppearanceCell.Options.UseTextOptions = true;
            this.col5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col5.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col5.AppearanceHeader.Options.UseFont = true;
            this.col5.AppearanceHeader.Options.UseTextOptions = true;
            this.col5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col5.FieldName = "5";
            this.col5.Name = "col5";
            this.col5.Visible = true;
            this.col5.VisibleIndex = 6;
            // 
            // col6
            // 
            this.col6.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col6.AppearanceCell.Options.UseFont = true;
            this.col6.AppearanceCell.Options.UseTextOptions = true;
            this.col6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col6.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col6.AppearanceHeader.Options.UseFont = true;
            this.col6.AppearanceHeader.Options.UseTextOptions = true;
            this.col6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col6.FieldName = "6";
            this.col6.Name = "col6";
            this.col6.Visible = true;
            this.col6.VisibleIndex = 7;
            // 
            // col7
            // 
            this.col7.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col7.AppearanceCell.Options.UseFont = true;
            this.col7.AppearanceCell.Options.UseTextOptions = true;
            this.col7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col7.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col7.AppearanceHeader.Options.UseFont = true;
            this.col7.AppearanceHeader.Options.UseTextOptions = true;
            this.col7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col7.FieldName = "7";
            this.col7.Name = "col7";
            this.col7.Visible = true;
            this.col7.VisibleIndex = 8;
            // 
            // col8
            // 
            this.col8.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col8.AppearanceCell.Options.UseFont = true;
            this.col8.AppearanceCell.Options.UseTextOptions = true;
            this.col8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col8.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col8.AppearanceHeader.Options.UseFont = true;
            this.col8.AppearanceHeader.Options.UseTextOptions = true;
            this.col8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col8.FieldName = "8";
            this.col8.Name = "col8";
            this.col8.Visible = true;
            this.col8.VisibleIndex = 9;
            // 
            // col9
            // 
            this.col9.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col9.AppearanceCell.Options.UseFont = true;
            this.col9.AppearanceCell.Options.UseTextOptions = true;
            this.col9.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col9.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col9.AppearanceHeader.Options.UseFont = true;
            this.col9.AppearanceHeader.Options.UseTextOptions = true;
            this.col9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col9.FieldName = "9";
            this.col9.Name = "col9";
            this.col9.Visible = true;
            this.col9.VisibleIndex = 10;
            // 
            // col10
            // 
            this.col10.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col10.AppearanceCell.Options.UseFont = true;
            this.col10.AppearanceCell.Options.UseTextOptions = true;
            this.col10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col10.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col10.AppearanceHeader.Options.UseFont = true;
            this.col10.AppearanceHeader.Options.UseTextOptions = true;
            this.col10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col10.FieldName = "10";
            this.col10.Name = "col10";
            this.col10.Visible = true;
            this.col10.VisibleIndex = 11;
            // 
            // col11
            // 
            this.col11.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col11.AppearanceCell.Options.UseFont = true;
            this.col11.AppearanceCell.Options.UseTextOptions = true;
            this.col11.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col11.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col11.AppearanceHeader.Options.UseFont = true;
            this.col11.AppearanceHeader.Options.UseTextOptions = true;
            this.col11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col11.FieldName = "11";
            this.col11.Name = "col11";
            this.col11.Visible = true;
            this.col11.VisibleIndex = 12;
            // 
            // col12
            // 
            this.col12.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col12.AppearanceCell.Options.UseFont = true;
            this.col12.AppearanceCell.Options.UseTextOptions = true;
            this.col12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col12.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col12.AppearanceHeader.Options.UseFont = true;
            this.col12.AppearanceHeader.Options.UseTextOptions = true;
            this.col12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col12.FieldName = "12";
            this.col12.Name = "col12";
            this.col12.Visible = true;
            this.col12.VisibleIndex = 13;
            // 
            // col13
            // 
            this.col13.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col13.AppearanceCell.Options.UseFont = true;
            this.col13.AppearanceCell.Options.UseTextOptions = true;
            this.col13.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col13.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col13.AppearanceHeader.Options.UseFont = true;
            this.col13.AppearanceHeader.Options.UseTextOptions = true;
            this.col13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col13.FieldName = "13";
            this.col13.Name = "col13";
            this.col13.Visible = true;
            this.col13.VisibleIndex = 14;
            // 
            // col14
            // 
            this.col14.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col14.AppearanceCell.Options.UseFont = true;
            this.col14.AppearanceCell.Options.UseTextOptions = true;
            this.col14.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col14.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col14.AppearanceHeader.Options.UseFont = true;
            this.col14.AppearanceHeader.Options.UseTextOptions = true;
            this.col14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col14.FieldName = "14";
            this.col14.Name = "col14";
            this.col14.Visible = true;
            this.col14.VisibleIndex = 15;
            // 
            // colالإمتحان
            // 
            this.colالإمتحان.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالإمتحان.AppearanceCell.Options.UseFont = true;
            this.colالإمتحان.AppearanceCell.Options.UseTextOptions = true;
            this.colالإمتحان.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالإمتحان.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالإمتحان.AppearanceHeader.Options.UseFont = true;
            this.colالإمتحان.AppearanceHeader.Options.UseTextOptions = true;
            this.colالإمتحان.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالإمتحان.FieldName = "الإمتحان";
            this.colالإمتحان.Name = "colالإمتحان";
            this.colالإمتحان.Visible = true;
            this.colالإمتحان.VisibleIndex = 16;
            // 
            // colالسعر
            // 
            this.colالسعر.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالسعر.AppearanceCell.Options.UseFont = true;
            this.colالسعر.AppearanceCell.Options.UseTextOptions = true;
            this.colالسعر.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالسعر.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالسعر.AppearanceHeader.Options.UseFont = true;
            this.colالسعر.AppearanceHeader.Options.UseTextOptions = true;
            this.colالسعر.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالسعر.FieldName = "السعر";
            this.colالسعر.Name = "colالسعر";
            this.colالسعر.OptionsColumn.AllowEdit = false;
            this.colالسعر.Visible = true;
            this.colالسعر.VisibleIndex = 17;
            // 
            // colمدفوع
            // 
            this.colمدفوع.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colمدفوع.AppearanceCell.Options.UseFont = true;
            this.colمدفوع.AppearanceCell.Options.UseTextOptions = true;
            this.colمدفوع.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colمدفوع.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colمدفوع.AppearanceHeader.Options.UseFont = true;
            this.colمدفوع.AppearanceHeader.Options.UseTextOptions = true;
            this.colمدفوع.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colمدفوع.FieldName = "مدفوع";
            this.colمدفوع.Name = "colمدفوع";
            this.colمدفوع.OptionsColumn.AllowEdit = false;
            this.colمدفوع.Visible = true;
            this.colمدفوع.VisibleIndex = 18;
            // 
            // colمتبقي
            // 
            this.colمتبقي.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colمتبقي.AppearanceCell.Options.UseFont = true;
            this.colمتبقي.AppearanceCell.Options.UseTextOptions = true;
            this.colمتبقي.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colمتبقي.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colمتبقي.AppearanceHeader.Options.UseFont = true;
            this.colمتبقي.AppearanceHeader.Options.UseTextOptions = true;
            this.colمتبقي.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colمتبقي.FieldName = "متبقي";
            this.colمتبقي.Name = "colمتبقي";
            this.colمتبقي.OptionsColumn.AllowEdit = false;
            this.colمتبقي.Visible = true;
            this.colمتبقي.VisibleIndex = 19;
            // 
            // gridColumn3
            // 
            this.gridColumn3.ColumnEdit = this.repositoryItemButtonEdit5;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 20;
            // 
            // repositoryItemButtonEdit5
            // 
            this.repositoryItemButtonEdit5.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemButtonEdit5.AutoHeight = false;
            this.repositoryItemButtonEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit5.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit5.Name = "repositoryItemButtonEdit5";
            this.repositoryItemButtonEdit5.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit5.Click += new System.EventHandler(this.repositoryItemButtonEdit5_Click);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl3;
            this.gridView1.Name = "gridView1";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1338, 551);
            this.xtraTabPage2.Text = "الطلاب";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.getStudentsOfGroupBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemButtonEdit2,
            this.repositoryItemButtonEdit3,
            this.repositoryItemButtonEdit4});
            this.gridControl2.Size = new System.Drawing.Size(1338, 551);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // getStudentsOfGroupBindingSource
            // 
            this.getStudentsOfGroupBindingSource.DataMember = "getStudentsOfGroup";
            this.getStudentsOfGroupBindingSource.DataSource = this.dsGetStudentsofGroup;
            // 
            // dsGetStudentsofGroup
            // 
            this.dsGetStudentsofGroup.DataSetName = "dsGetStudentsofGroup";
            this.dsGetStudentsofGroup.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.ColumnPanelRowHeight = 50;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colم1,
            this.colالإسم,
            this.colرقمالكشف1,
            this.colالنوع,
            this.colسعرالشهر,
            this.gridColumn1,
            this.gridColumn2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.RowHeight = 50;
            // 
            // colم1
            // 
            this.colم1.FieldName = "م";
            this.colم1.Name = "colم1";
            // 
            // colالإسم
            // 
            this.colالإسم.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالإسم.AppearanceCell.Options.UseFont = true;
            this.colالإسم.AppearanceCell.Options.UseTextOptions = true;
            this.colالإسم.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالإسم.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالإسم.AppearanceHeader.Options.UseFont = true;
            this.colالإسم.AppearanceHeader.Options.UseTextOptions = true;
            this.colالإسم.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالإسم.FieldName = "الإسم";
            this.colالإسم.MinWidth = 10;
            this.colالإسم.Name = "colالإسم";
            this.colالإسم.OptionsColumn.AllowEdit = false;
            this.colالإسم.Visible = true;
            this.colالإسم.VisibleIndex = 1;
            this.colالإسم.Width = 176;
            // 
            // colرقمالكشف1
            // 
            this.colرقمالكشف1.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colرقمالكشف1.AppearanceCell.Options.UseFont = true;
            this.colرقمالكشف1.AppearanceCell.Options.UseTextOptions = true;
            this.colرقمالكشف1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colرقمالكشف1.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colرقمالكشف1.AppearanceHeader.Options.UseFont = true;
            this.colرقمالكشف1.AppearanceHeader.Options.UseTextOptions = true;
            this.colرقمالكشف1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colرقمالكشف1.FieldName = "رقم الكشف";
            this.colرقمالكشف1.MinWidth = 10;
            this.colرقمالكشف1.Name = "colرقمالكشف1";
            this.colرقمالكشف1.OptionsColumn.AllowEdit = false;
            this.colرقمالكشف1.Visible = true;
            this.colرقمالكشف1.VisibleIndex = 0;
            this.colرقمالكشف1.Width = 109;
            // 
            // colالنوع
            // 
            this.colالنوع.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالنوع.AppearanceCell.Options.UseFont = true;
            this.colالنوع.AppearanceCell.Options.UseTextOptions = true;
            this.colالنوع.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالنوع.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colالنوع.AppearanceHeader.Options.UseFont = true;
            this.colالنوع.AppearanceHeader.Options.UseTextOptions = true;
            this.colالنوع.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colالنوع.FieldName = "النوع";
            this.colالنوع.Name = "colالنوع";
            this.colالنوع.OptionsColumn.AllowEdit = false;
            this.colالنوع.Visible = true;
            this.colالنوع.VisibleIndex = 2;
            this.colالنوع.Width = 176;
            // 
            // colسعرالشهر
            // 
            this.colسعرالشهر.AppearanceCell.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colسعرالشهر.AppearanceCell.Options.UseFont = true;
            this.colسعرالشهر.AppearanceCell.Options.UseTextOptions = true;
            this.colسعرالشهر.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colسعرالشهر.AppearanceHeader.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colسعرالشهر.AppearanceHeader.Options.UseFont = true;
            this.colسعرالشهر.AppearanceHeader.Options.UseTextOptions = true;
            this.colسعرالشهر.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colسعرالشهر.FieldName = "سعر الشهر";
            this.colسعرالشهر.Name = "colسعرالشهر";
            this.colسعرالشهر.OptionsColumn.AllowEdit = false;
            this.colسعرالشهر.Visible = true;
            this.colسعرالشهر.VisibleIndex = 3;
            this.colسعرالشهر.Width = 176;
            // 
            // gridColumn1
            // 
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit2;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 4;
            this.gridColumn1.Width = 176;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemButtonEdit2.AutoHeight = false;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit2.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.Click += new System.EventHandler(this.repositoryItemButtonEdit2_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.ColumnEdit = this.repositoryItemButtonEdit3;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 5;
            this.gridColumn2.Width = 190;
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemButtonEdit3.AutoHeight = false;
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit3.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            this.repositoryItemButtonEdit3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit3.Click += new System.EventHandler(this.repositoryItemButtonEdit3_Click);
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // repositoryItemButtonEdit4
            // 
            this.repositoryItemButtonEdit4.AutoHeight = false;
            this.repositoryItemButtonEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit4.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.repositoryItemButtonEdit4.Name = "repositoryItemButtonEdit4";
            this.repositoryItemButtonEdit4.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 42);
            this.gridControl1.MainView = this.gridView4;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(597, 268);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4,
            this.gridView6});
            // 
            // gridView4
            // 
            this.gridView4.ColumnPanelRowHeight = 40;
            this.gridView4.GridControl = this.gridControl1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.Editable = false;
            this.gridView4.RowHeight = 30;
            // 
            // gridView6
            // 
            this.gridView6.GridControl = this.gridControl1;
            this.gridView6.Name = "gridView6";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.groupControl5);
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Controls.Add(this.dataLayoutControl1);
            this.xtraTabPage1.Controls.Add(this.btnAddStudent);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1338, 551);
            this.xtraTabPage1.Text = "بيانات المجموعة";
            // 
            // groupControl5
            // 
            this.groupControl5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupControl5.AppearanceCaption.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl5.AppearanceCaption.Options.UseFont = true;
            this.groupControl5.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl5.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl5.Controls.Add(this.gridControl1);
            this.groupControl5.Location = new System.Drawing.Point(585, 13);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(601, 312);
            this.groupControl5.TabIndex = 12;
            this.groupControl5.Text = "المواعيد";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.Controls.Add(this.btnDeleteDate);
            this.groupControl1.Controls.Add(this.btnDeleteGroup);
            this.groupControl1.Controls.Add(this.btnAddDate);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Location = new System.Drawing.Point(587, 331);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(599, 217);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "عمليات متاحة";
            // 
            // btnDeleteDate
            // 
            this.btnDeleteDate.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnDeleteDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteDate.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteDate.Appearance.Options.UseFont = true;
            this.btnDeleteDate.Enabled = false;
            this.btnDeleteDate.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteDate.Image")));
            this.btnDeleteDate.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnDeleteDate.Location = new System.Drawing.Point(175, 42);
            this.btnDeleteDate.Name = "btnDeleteDate";
            this.btnDeleteDate.Size = new System.Drawing.Size(195, 57);
            this.btnDeleteDate.TabIndex = 4;
            this.btnDeleteDate.Click += new System.EventHandler(this.btnDeleteDate_Click);
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnDeleteGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteGroup.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteGroup.Appearance.Options.UseFont = true;
            this.btnDeleteGroup.Enabled = false;
            this.btnDeleteGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteGroup.Image")));
            this.btnDeleteGroup.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnDeleteGroup.Location = new System.Drawing.Point(9, 42);
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Size = new System.Drawing.Size(160, 57);
            this.btnDeleteGroup.TabIndex = 3;
            this.btnDeleteGroup.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            // 
            // btnAddDate
            // 
            this.btnAddDate.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnAddDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddDate.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddDate.Appearance.Options.UseFont = true;
            this.btnAddDate.Enabled = false;
            this.btnAddDate.Image = ((System.Drawing.Image)(resources.GetObject("btnAddDate.Image")));
            this.btnAddDate.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAddDate.Location = new System.Drawing.Point(376, 42);
            this.btnAddDate.Name = "btnAddDate";
            this.btnAddDate.Size = new System.Drawing.Size(213, 57);
            this.btnAddDate.TabIndex = 2;
            this.btnAddDate.Click += new System.EventHandler(this.btnAddDate_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl2.Controls.Add(this.teTime);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.btnSave);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.cmbDays);
            this.groupControl2.Location = new System.Drawing.Point(4, 113);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(590, 91);
            this.groupControl2.TabIndex = 4;
            this.groupControl2.Text = "المعاد";
            this.groupControl2.Visible = false;
            // 
            // teTime
            // 
            this.teTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.teTime.EditValue = new System.DateTime(2016, 12, 2, 0, 0, 0, 0);
            this.teTime.Location = new System.Drawing.Point(171, 49);
            this.teTime.Name = "teTime";
            this.teTime.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teTime.Properties.Appearance.Options.UseFont = true;
            this.teTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teTime.Properties.Mask.EditMask = "t";
            this.teTime.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.teTime.Size = new System.Drawing.Size(128, 34);
            this.teTime.TabIndex = 69;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(305, 52);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(54, 29);
            this.labelControl13.TabIndex = 67;
            this.labelControl13.Text = "الساعة";
            // 
            // btnSave
            // 
            this.btnSave.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnSave.Location = new System.Drawing.Point(5, 40);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(160, 49);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(546, 52);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(37, 29);
            this.labelControl14.TabIndex = 66;
            this.labelControl14.Text = "اليوم";
            // 
            // cmbDays
            // 
            this.cmbDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDays.EditValue = "السبت";
            this.cmbDays.Location = new System.Drawing.Point(372, 49);
            this.cmbDays.Name = "cmbDays";
            this.cmbDays.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDays.Properties.Appearance.Options.UseFont = true;
            this.cmbDays.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDays.Properties.Items.AddRange(new object[] {
            "السبت",
            "الأحد",
            "الإثنين",
            "الثلاثاء",
            "الأربعاء",
            "الخميس",
            "الجمعة"});
            this.cmbDays.Properties.PopupSizeable = true;
            this.cmbDays.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbDays.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbDays.Size = new System.Drawing.Size(168, 34);
            this.cmbDays.TabIndex = 68;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataLayoutControl1.Controls.Add(this.btnSaveChanges);
            this.dataLayoutControl1.Controls.Add(this.nameGroupTextEdit);
            this.dataLayoutControl1.Controls.Add(this.priceTextEdit);
            this.dataLayoutControl1.Location = new System.Drawing.Point(46, 35);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(539, 181);
            this.dataLayoutControl1.TabIndex = 11;
            this.dataLayoutControl1.Text = "بيانات المجموعة";
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSaveChanges.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveChanges.Appearance.Options.UseFont = true;
            this.btnSaveChanges.Location = new System.Drawing.Point(31, 111);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(477, 38);
            this.btnSaveChanges.StyleController = this.dataLayoutControl1;
            this.btnSaveChanges.TabIndex = 12;
            this.btnSaveChanges.Text = "تعديل";
            this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
            // 
            // nameGroupTextEdit
            // 
            this.nameGroupTextEdit.Location = new System.Drawing.Point(235, 71);
            this.nameGroupTextEdit.Name = "nameGroupTextEdit";
            this.nameGroupTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameGroupTextEdit.Properties.Appearance.Options.UseFont = true;
            this.nameGroupTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.nameGroupTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameGroupTextEdit.Properties.ReadOnly = true;
            this.nameGroupTextEdit.Size = new System.Drawing.Size(147, 34);
            this.nameGroupTextEdit.StyleController = this.dataLayoutControl1;
            this.nameGroupTextEdit.TabIndex = 4;
            // 
            // priceTextEdit
            // 
            this.priceTextEdit.Location = new System.Drawing.Point(31, 71);
            this.priceTextEdit.Name = "priceTextEdit";
            this.priceTextEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceTextEdit.Properties.Appearance.Options.UseFont = true;
            this.priceTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.priceTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.priceTextEdit.Properties.Mask.EditMask = "F";
            this.priceTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.priceTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.priceTextEdit.Properties.ReadOnly = true;
            this.priceTextEdit.Size = new System.Drawing.Size(72, 34);
            this.priceTextEdit.StyleController = this.dataLayoutControl1;
            this.priceTextEdit.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(539, 181);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup2.Size = new System.Drawing.Size(513, 155);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup3.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemFornameGroup,
            this.ItemForprice,
            this.layoutControlItem1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup3.Size = new System.Drawing.Size(513, 155);
            this.layoutControlGroup3.Text = "بيانات المجموعة";
            // 
            // ItemFornameGroup
            // 
            this.ItemFornameGroup.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemFornameGroup.AppearanceItemCaption.Options.UseFont = true;
            this.ItemFornameGroup.Control = this.nameGroupTextEdit;
            this.ItemFornameGroup.Location = new System.Drawing.Point(204, 0);
            this.ItemFornameGroup.Name = "ItemFornameGroup";
            this.ItemFornameGroup.Size = new System.Drawing.Size(279, 40);
            this.ItemFornameGroup.Text = "إسم المجموعة";
            this.ItemFornameGroup.TextSize = new System.Drawing.Size(122, 29);
            // 
            // ItemForprice
            // 
            this.ItemForprice.AppearanceItemCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemForprice.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForprice.Control = this.priceTextEdit;
            this.ItemForprice.Location = new System.Drawing.Point(0, 0);
            this.ItemForprice.Name = "ItemForprice";
            this.ItemForprice.Size = new System.Drawing.Size(204, 40);
            this.ItemForprice.Text = "السعر";
            this.ItemForprice.TextSize = new System.Drawing.Size(122, 29);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnSaveChanges;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(483, 45);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnAddStudent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddStudent.Appearance.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStudent.Appearance.Options.UseFont = true;
            this.btnAddStudent.Enabled = false;
            this.btnAddStudent.Image = ((System.Drawing.Image)(resources.GetObject("btnAddStudent.Image")));
            this.btnAddStudent.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnAddStudent.Location = new System.Drawing.Point(312, 319);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(237, 207);
            this.btnAddStudent.TabIndex = 10;
            this.btnAddStudent.Text = "إضافة طالب";
            this.btnAddStudent.Click += new System.EventHandler(this.btnAddStudent_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.Location = new System.Drawing.Point(5, 58);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1345, 604);
            this.xtraTabControl1.TabIndex = 9;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // getAbsenceOfGroupTableAdapter
            // 
            this.getAbsenceOfGroupTableAdapter.ClearBeforeFill = true;
            // 
            // getStudentsOfGroupTableAdapter
            // 
            this.getStudentsOfGroupTableAdapter.ClearBeforeFill = true;
            // 
            // frmGroups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 663);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.cmbGroup);
            this.Controls.Add(this.cmbClass);
            this.Controls.Add(this.cmbStage);
            this.Name = "frmGroups";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "عرض المجموعات";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmGroups_Load);
            this.Shown += new System.EventHandler(this.frmGroups_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.cmbStage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroup.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getAbsenceOfGroupBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGetAbsenceOfGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getStudentsOfGroupBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGetStudentsofGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nameGroupTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFornameGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForprice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colhour;
        private DevExpress.XtraGrid.Columns.GridColumn colday;
        private DevExpress.XtraGrid.Columns.GridColumn colnameGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colnameClass;
        private DevExpress.XtraGrid.Columns.GridColumn colnameStage;
        private DevExpress.XtraGrid.Columns.GridColumn colnameTeacher;
        private DevExpress.XtraEditors.LookUpEdit cmbStage;
        private DevExpress.XtraEditors.LookUpEdit cmbClass;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.SimpleButton btnReport;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnDeleteDate;
        private DevExpress.XtraEditors.SimpleButton btnDeleteGroup;
        private DevExpress.XtraEditors.SimpleButton btnAddDate;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TimeEdit teTime;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.ComboBoxEdit cmbDays;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnRepGroup;
        private System.Windows.Forms.BindingSource getAbsenceOfGroupBindingSource;
        private dsGetAbsenceOfGroup dsGetAbsenceOfGroup;
        private dsGetAbsenceOfGroupTableAdapters.getAbsenceOfGroupTableAdapter getAbsenceOfGroupTableAdapter;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private System.Windows.Forms.BindingSource getStudentsOfGroupBindingSource;
        private dsGetStudentsofGroup dsGetStudentsofGroup;
        private dsGetStudentsofGroupTableAdapters.getStudentsOfGroupTableAdapter getStudentsOfGroupTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnPrintGrid;
        private DevExpress.XtraEditors.SimpleButton btnAddStudent;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colم1;
        private DevExpress.XtraGrid.Columns.GridColumn colالإسم;
        private DevExpress.XtraGrid.Columns.GridColumn colرقمالكشف1;
        private DevExpress.XtraGrid.Columns.GridColumn colالنوع;
        private DevExpress.XtraGrid.Columns.GridColumn colسعرالشهر;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colم;
        private DevExpress.XtraGrid.Columns.GridColumn colرقمالكشف;
        private DevExpress.XtraGrid.Columns.GridColumn colالطالب;
        private DevExpress.XtraGrid.Columns.GridColumn colالشهر;
        private DevExpress.XtraGrid.Columns.GridColumn col1;
        private DevExpress.XtraGrid.Columns.GridColumn col2;
        private DevExpress.XtraGrid.Columns.GridColumn col3;
        private DevExpress.XtraGrid.Columns.GridColumn col4;
        private DevExpress.XtraGrid.Columns.GridColumn col5;
        private DevExpress.XtraGrid.Columns.GridColumn col6;
        private DevExpress.XtraGrid.Columns.GridColumn col7;
        private DevExpress.XtraGrid.Columns.GridColumn col8;
        private DevExpress.XtraGrid.Columns.GridColumn col9;
        private DevExpress.XtraGrid.Columns.GridColumn col10;
        private DevExpress.XtraGrid.Columns.GridColumn col11;
        private DevExpress.XtraGrid.Columns.GridColumn col12;
        private DevExpress.XtraGrid.Columns.GridColumn col13;
        private DevExpress.XtraGrid.Columns.GridColumn col14;
        private DevExpress.XtraGrid.Columns.GridColumn colالإمتحان;
        private DevExpress.XtraGrid.Columns.GridColumn colالسعر;
        private DevExpress.XtraGrid.Columns.GridColumn colمدفوع;
        private DevExpress.XtraGrid.Columns.GridColumn colمتبقي;
        private DevExpress.XtraEditors.SimpleButton btnSaveChanges;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit nameGroupTextEdit;
        private DevExpress.XtraEditors.TextEdit priceTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemFornameGroup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForprice;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit5;
        public DevExpress.XtraEditors.LookUpEdit cmbGroup;
    }
}