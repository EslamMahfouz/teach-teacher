﻿namespace Teach.PL
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.btnCenterData = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrintingSettings = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem19 = new DevExpress.XtraBars.BarSubItem();
            this.btnAddGroup = new DevExpress.XtraBars.BarButtonItem();
            this.btnShowGroups = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem20 = new DevExpress.XtraBars.BarSubItem();
            this.btnChangeGroup = new DevExpress.XtraBars.BarButtonItem();
            this.btnDepits = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem21 = new DevExpress.XtraBars.BarSubItem();
            this.btnAddUser = new DevExpress.XtraBars.BarButtonItem();
            this.btnEditUser = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem23 = new DevExpress.XtraBars.BarSubItem();
            this.btnIncome = new DevExpress.XtraBars.BarButtonItem();
            this.barUser = new DevExpress.XtraBars.BarSubItem();
            this.btnChangePassword = new DevExpress.XtraBars.BarButtonItem();
            this.btnLogout = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.skinBarSubItem3 = new DevExpress.XtraBars.SkinBarSubItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnBackup = new DevExpress.XtraBars.BarButtonItem();
            this.btnRestore = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.btnShowC1 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem8 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem9 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem10 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem11 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem12 = new DevExpress.XtraBars.BarSubItem();
            this.btnShowC6 = new DevExpress.XtraBars.BarButtonItem();
            this.btnEditDates = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barLinkContainerItem8 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem9 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem6 = new DevExpress.XtraBars.BarSubItem();
            this.skinBarSubItem1 = new DevExpress.XtraBars.SkinBarSubItem();
            this.skinBarSubItem2 = new DevExpress.XtraBars.SkinBarSubItem();
            this.barSubItem7 = new DevExpress.XtraBars.BarSubItem();
            this.barLinkContainerItem2 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem13 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem14 = new DevExpress.XtraBars.BarSubItem();
            this.btnEditGroup = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem15 = new DevExpress.XtraBars.BarSubItem();
            this.btnMonthlyReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem16 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem17 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem18 = new DevExpress.XtraBars.BarSubItem();
            this.btnAddTeacher = new DevExpress.XtraBars.BarButtonItem();
            this.btnLoginSettings = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barWorkspaceMenuItem1 = new DevExpress.XtraBars.BarWorkspaceMenuItem();
            this.barWorkspaceMenuItem2 = new DevExpress.XtraBars.BarWorkspaceMenuItem();
            this.barSubItem22 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.navBarGroup4 = new DevExpress.XtraNavBar.NavBarGroup();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.cardView1 = new DevExpress.XtraGrid.Views.Card.CardView();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.btnBackup,
            this.btnRestore,
            this.barSubItem2,
            this.btnEditDates,
            this.barSubItem4,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem11,
            this.barButtonItem12,
            this.barButtonItem13,
            this.barButtonItem14,
            this.barLinkContainerItem1,
            this.barLinkContainerItem8,
            this.barButtonItem15,
            this.barButtonItem16,
            this.barButtonItem17,
            this.barButtonItem18,
            this.barLinkContainerItem9,
            this.barSubItem5,
            this.barButtonItem20,
            this.barSubItem6,
            this.skinBarSubItem1,
            this.skinBarSubItem2,
            this.skinBarSubItem3,
            this.barSubItem7,
            this.barLinkContainerItem2,
            this.btnShowC1,
            this.barSubItem8,
            this.barSubItem9,
            this.barSubItem10,
            this.barSubItem11,
            this.barSubItem12,
            this.btnShowC6,
            this.barSubItem3,
            this.barSubItem13,
            this.barSubItem14,
            this.btnEditGroup,
            this.barSubItem15,
            this.barButtonItem9,
            this.barSubItem16,
            this.barSubItem17,
            this.barButtonItem10,
            this.btnMonthlyReport,
            this.barSubItem18,
            this.btnAddTeacher,
            this.btnPrintingSettings,
            this.btnCenterData,
            this.barStaticItem1,
            this.barSubItem19,
            this.barSubItem20,
            this.btnAddGroup,
            this.btnShowGroups,
            this.btnChangeGroup,
            this.btnDepits,
            this.btnLoginSettings,
            this.barButtonItem1,
            this.barSubItem21,
            this.barWorkspaceMenuItem1,
            this.barWorkspaceMenuItem2,
            this.barUser,
            this.btnAddUser,
            this.btnEditUser,
            this.barSubItem22,
            this.barButtonItem21,
            this.barButtonItem22,
            this.barButtonItem2,
            this.barSubItem23,
            this.btnIncome,
            this.btnChangePassword,
            this.btnLogout});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 319;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem19),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem20),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.barUser)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.barSubItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.barSubItem1.Caption = "إعدادات البرنامج";
            this.barSubItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem1.Glyph")));
            this.barSubItem1.Id = 0;
            this.barSubItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem1.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem1.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCenterData),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnPrintingSettings, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnCenterData
            // 
            this.btnCenterData.Caption = "بيانات عامة";
            this.btnCenterData.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCenterData.Glyph")));
            this.btnCenterData.Id = 78;
            this.btnCenterData.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnCenterData.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCenterData.Name = "btnCenterData";
            this.btnCenterData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnCenterData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCenterData_ItemClick);
            // 
            // btnPrintingSettings
            // 
            this.btnPrintingSettings.Caption = "إعدادات الطباعة";
            this.btnPrintingSettings.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPrintingSettings.Glyph")));
            this.btnPrintingSettings.Id = 77;
            this.btnPrintingSettings.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnPrintingSettings.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPrintingSettings.Name = "btnPrintingSettings";
            this.btnPrintingSettings.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnPrintingSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrintingSettings_ItemClick);
            // 
            // barSubItem19
            // 
            this.barSubItem19.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.barSubItem19.Caption = "المجموعات";
            this.barSubItem19.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem19.Glyph")));
            this.barSubItem19.Id = 88;
            this.barSubItem19.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem19.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem19.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAddGroup),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, this.btnShowGroups, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "G", "")});
            this.barSubItem19.Name = "barSubItem19";
            this.barSubItem19.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnAddGroup
            // 
            this.btnAddGroup.Caption = "إضافة مجموعة";
            this.btnAddGroup.Glyph = ((System.Drawing.Image)(resources.GetObject("btnAddGroup.Glyph")));
            this.btnAddGroup.Id = 90;
            this.btnAddGroup.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddGroup.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAddGroup.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A));
            this.btnAddGroup.Name = "btnAddGroup";
            this.btnAddGroup.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnAddGroup.ShortcutKeyDisplayString = "Ctrl+A";
            this.btnAddGroup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddGroup_ItemClick);
            // 
            // btnShowGroups
            // 
            this.btnShowGroups.Caption = "عرض المجموعات";
            this.btnShowGroups.Glyph = ((System.Drawing.Image)(resources.GetObject("btnShowGroups.Glyph")));
            this.btnShowGroups.Id = 91;
            this.btnShowGroups.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowGroups.ItemAppearance.Normal.Options.UseFont = true;
            this.btnShowGroups.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G));
            this.btnShowGroups.Name = "btnShowGroups";
            this.btnShowGroups.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnShowGroups.ShortcutKeyDisplayString = "Ctrl+G";
            this.btnShowGroups.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnShowGroups_ItemClick);
            // 
            // barSubItem20
            // 
            this.barSubItem20.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.barSubItem20.Caption = "الطلاب";
            this.barSubItem20.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem20.Glyph")));
            this.barSubItem20.Id = 89;
            this.barSubItem20.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem20.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem20.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnChangeGroup),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDepits, DevExpress.XtraBars.BarItemPaintStyle.Standard)});
            this.barSubItem20.Name = "barSubItem20";
            this.barSubItem20.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnChangeGroup
            // 
            this.btnChangeGroup.Caption = "تغيير مجموعة طالب";
            this.btnChangeGroup.Glyph = ((System.Drawing.Image)(resources.GetObject("btnChangeGroup.Glyph")));
            this.btnChangeGroup.Id = 92;
            this.btnChangeGroup.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnChangeGroup.ItemAppearance.Normal.Options.UseFont = true;
            this.btnChangeGroup.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C));
            this.btnChangeGroup.Name = "btnChangeGroup";
            this.btnChangeGroup.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnChangeGroup.ShortcutKeyDisplayString = "Ctrl+C";
            this.btnChangeGroup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnChangeGroup_ItemClick);
            // 
            // btnDepits
            // 
            this.btnDepits.Caption = "المدينيين";
            this.btnDepits.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDepits.Glyph")));
            this.btnDepits.Id = 93;
            this.btnDepits.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnDepits.ItemAppearance.Normal.Options.UseFont = true;
            this.btnDepits.Name = "btnDepits";
            this.btnDepits.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnDepits.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDepits_ItemClick);
            // 
            // barSubItem21
            // 
            this.barSubItem21.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.barSubItem21.Caption = "المستخدمين";
            this.barSubItem21.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem21.Glyph")));
            this.barSubItem21.Id = 96;
            this.barSubItem21.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem21.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem21.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Verdana", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem21.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barSubItem21.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAddUser),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEditUser)});
            this.barSubItem21.Name = "barSubItem21";
            this.barSubItem21.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnAddUser
            // 
            this.btnAddUser.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnAddUser.Caption = "إضافة مستخدم";
            this.btnAddUser.Glyph = ((System.Drawing.Image)(resources.GetObject("btnAddUser.Glyph")));
            this.btnAddUser.Id = 100;
            this.btnAddUser.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnAddUser.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnAddUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddUser_ItemClick);
            // 
            // btnEditUser
            // 
            this.btnEditUser.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnEditUser.Caption = "عرض وتعديل المستخدمين";
            this.btnEditUser.Glyph = ((System.Drawing.Image)(resources.GetObject("btnEditUser.Glyph")));
            this.btnEditUser.Id = 101;
            this.btnEditUser.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnEditUser.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnEditUser.Name = "btnEditUser";
            this.btnEditUser.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnEditUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEditUser_ItemClick);
            // 
            // barSubItem23
            // 
            this.barSubItem23.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.barSubItem23.Caption = "التقارير";
            this.barSubItem23.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem23.Glyph")));
            this.barSubItem23.Id = 315;
            this.barSubItem23.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem23.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem23.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnIncome)});
            this.barSubItem23.Name = "barSubItem23";
            this.barSubItem23.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnIncome
            // 
            this.btnIncome.Caption = "الإيرادات";
            this.btnIncome.Glyph = ((System.Drawing.Image)(resources.GetObject("btnIncome.Glyph")));
            this.btnIncome.Id = 316;
            this.btnIncome.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncome.ItemAppearance.Normal.Options.UseFont = true;
            this.btnIncome.Name = "btnIncome";
            this.btnIncome.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnIncome_ItemClick);
            // 
            // barUser
            // 
            this.barUser.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.barUser.Glyph = ((System.Drawing.Image)(resources.GetObject("barUser.Glyph")));
            this.barUser.Id = 99;
            this.barUser.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barUser.ItemAppearance.Normal.Options.UseFont = true;
            this.barUser.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnChangePassword),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnLogout)});
            this.barUser.Name = "barUser";
            this.barUser.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnChangePassword.Caption = "تغيير الرقم السري";
            this.btnChangePassword.Glyph = ((System.Drawing.Image)(resources.GetObject("btnChangePassword.Glyph")));
            this.btnChangePassword.Id = 317;
            this.btnChangePassword.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePassword.ItemAppearance.Normal.Options.UseFont = true;
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnChangePassword_ItemClick);
            // 
            // btnLogout
            // 
            this.btnLogout.Caption = "تسجيل خروج";
            this.btnLogout.Glyph = ((System.Drawing.Image)(resources.GetObject("btnLogout.Glyph")));
            this.btnLogout.Id = 318;
            this.btnLogout.ItemAppearance.Normal.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ItemAppearance.Normal.Options.UseFont = true;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLogout_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.skinBarSubItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // skinBarSubItem3
            // 
            this.skinBarSubItem3.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.skinBarSubItem3.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.skinBarSubItem3.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.skinBarSubItem3.Caption = "Themes";
            this.skinBarSubItem3.Id = 38;
            this.skinBarSubItem3.Name = "skinBarSubItem3";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "تم التصميم بواسطة م/ إسلام محفوظ، محمول 01003667015";
            this.barStaticItem1.Id = 87;
            this.barStaticItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.barStaticItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1278, 72);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 483);
            this.barDockControlBottom.Size = new System.Drawing.Size(1278, 34);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 72);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 411);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1278, 72);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 411);
            // 
            // btnBackup
            // 
            this.btnBackup.Id = 79;
            this.btnBackup.Name = "btnBackup";
            // 
            // btnRestore
            // 
            this.btnRestore.Id = 80;
            this.btnRestore.Name = "btnRestore";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.barSubItem2.Caption = "المرحلة الإبتدائية";
            this.barSubItem2.Id = 4;
            this.barSubItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem11),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem12)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "الصف الأول";
            this.barSubItem5.Id = 32;
            this.barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnShowC1)});
            this.barSubItem5.Name = "barSubItem5";
            // 
            // btnShowC1
            // 
            this.btnShowC1.Caption = "المجموعات";
            this.btnShowC1.Id = 43;
            this.btnShowC1.Name = "btnShowC1";
            // 
            // barSubItem8
            // 
            this.barSubItem8.Caption = "الصف الثاني";
            this.barSubItem8.Id = 44;
            this.barSubItem8.Name = "barSubItem8";
            // 
            // barSubItem9
            // 
            this.barSubItem9.Caption = "الصف الثالث";
            this.barSubItem9.Id = 45;
            this.barSubItem9.Name = "barSubItem9";
            // 
            // barSubItem10
            // 
            this.barSubItem10.Caption = "الصف الرابع";
            this.barSubItem10.Id = 46;
            this.barSubItem10.Name = "barSubItem10";
            // 
            // barSubItem11
            // 
            this.barSubItem11.Caption = "الصف الخامس";
            this.barSubItem11.Id = 47;
            this.barSubItem11.Name = "barSubItem11";
            // 
            // barSubItem12
            // 
            this.barSubItem12.Caption = "الصف السادس";
            this.barSubItem12.Id = 48;
            this.barSubItem12.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnShowC6)});
            this.barSubItem12.Name = "barSubItem12";
            // 
            // btnShowC6
            // 
            this.btnShowC6.Caption = "المجموعات";
            this.btnShowC6.Id = 49;
            this.btnShowC6.Name = "btnShowC6";
            // 
            // btnEditDates
            // 
            this.btnEditDates.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.btnEditDates.Caption = "المجموعات";
            this.btnEditDates.Id = 5;
            this.btnEditDates.ImageUri.Uri = "Add";
            this.btnEditDates.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditDates.ItemAppearance.Normal.Options.UseFont = true;
            this.btnEditDates.Name = "btnEditDates";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.barSubItem4.Caption = "المرحلة الثانوية";
            this.barSubItem4.Id = 6;
            this.barSubItem4.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem4.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem12),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem13),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem14)});
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "الصف الأول";
            this.barButtonItem12.Id = 16;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "الصف الثاني";
            this.barButtonItem13.Id = 17;
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "الصف الثالث";
            this.barButtonItem14.Id = 18;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "الصف الأول";
            this.barButtonItem3.Id = 7;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "الصف الثاني";
            this.barButtonItem4.Id = 8;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "الصف الثالث";
            this.barButtonItem5.Id = 9;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "الصف الرابع";
            this.barButtonItem6.Id = 10;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "الصف الخامس";
            this.barButtonItem7.Id = 11;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "الصف السادس";
            this.barButtonItem8.Id = 12;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "تعديل مواعيد المجموعات";
            this.barButtonItem11.Id = 15;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "الصفوف";
            this.barLinkContainerItem1.Id = 19;
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // barLinkContainerItem8
            // 
            this.barLinkContainerItem8.Caption = "المجموعات";
            this.barLinkContainerItem8.Id = 26;
            this.barLinkContainerItem8.Name = "barLinkContainerItem8";
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "إضافة مجموعة";
            this.barButtonItem15.Id = 27;
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "إضافة طالب إلي مجموعة";
            this.barButtonItem16.Id = 28;
            this.barButtonItem16.Name = "barButtonItem16";
            // 
            // barButtonItem17
            // 
            this.barButtonItem17.Caption = "إضافة طالب إلي مجموعة";
            this.barButtonItem17.Id = 29;
            this.barButtonItem17.Name = "barButtonItem17";
            // 
            // barButtonItem18
            // 
            this.barButtonItem18.Caption = "إضافة مجموعة";
            this.barButtonItem18.Id = 30;
            this.barButtonItem18.Name = "barButtonItem18";
            // 
            // barLinkContainerItem9
            // 
            this.barLinkContainerItem9.Caption = "المجموعات";
            this.barLinkContainerItem9.Id = 31;
            this.barLinkContainerItem9.Name = "barLinkContainerItem9";
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "إضافة مجموعة";
            this.barButtonItem20.Id = 34;
            this.barButtonItem20.Name = "barButtonItem20";
            // 
            // barSubItem6
            // 
            this.barSubItem6.Caption = "المجموعات";
            this.barSubItem6.Id = 35;
            this.barSubItem6.Name = "barSubItem6";
            // 
            // skinBarSubItem1
            // 
            this.skinBarSubItem1.Caption = "Themes";
            this.skinBarSubItem1.Id = 36;
            this.skinBarSubItem1.Name = "skinBarSubItem1";
            // 
            // skinBarSubItem2
            // 
            this.skinBarSubItem2.Caption = "skinBarSubItem2";
            this.skinBarSubItem2.Id = 37;
            this.skinBarSubItem2.Name = "skinBarSubItem2";
            // 
            // barSubItem7
            // 
            this.barSubItem7.Id = 86;
            this.barSubItem7.Name = "barSubItem7";
            // 
            // barLinkContainerItem2
            // 
            this.barLinkContainerItem2.Caption = "containerS1C1";
            this.barLinkContainerItem2.Id = 42;
            this.barLinkContainerItem2.Name = "barLinkContainerItem2";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Id = 82;
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barSubItem13
            // 
            this.barSubItem13.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.barSubItem13.Caption = "الدفع";
            this.barSubItem13.Id = 56;
            this.barSubItem13.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem13.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem13.Name = "barSubItem13";
            // 
            // barSubItem14
            // 
            this.barSubItem14.Id = 85;
            this.barSubItem14.Name = "barSubItem14";
            // 
            // btnEditGroup
            // 
            this.btnEditGroup.Caption = "تعديل مواعيد";
            this.btnEditGroup.Id = 60;
            this.btnEditGroup.Name = "btnEditGroup";
            // 
            // barSubItem15
            // 
            this.barSubItem15.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.barSubItem15.Caption = "التقارير";
            this.barSubItem15.Id = 62;
            this.barSubItem15.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem15.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem15.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMonthlyReport)});
            this.barSubItem15.Name = "barSubItem15";
            // 
            // btnMonthlyReport
            // 
            this.btnMonthlyReport.Caption = "تقرير شهري";
            this.btnMonthlyReport.Id = 67;
            this.btnMonthlyReport.Name = "btnMonthlyReport";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "تقارير لأولياء الأمور";
            this.barButtonItem9.Id = 63;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barSubItem16
            // 
            this.barSubItem16.Caption = "تقرير شهري";
            this.barSubItem16.Id = 64;
            this.barSubItem16.Name = "barSubItem16";
            // 
            // barSubItem17
            // 
            this.barSubItem17.Caption = "تقارير لأولياء الأمور";
            this.barSubItem17.Id = 65;
            this.barSubItem17.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem10)});
            this.barSubItem17.Name = "barSubItem17";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "تقرير شهري";
            this.barButtonItem10.Id = 66;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barSubItem18
            // 
            this.barSubItem18.Id = 83;
            this.barSubItem18.Name = "barSubItem18";
            // 
            // btnAddTeacher
            // 
            this.btnAddTeacher.Id = 84;
            this.btnAddTeacher.Name = "btnAddTeacher";
            // 
            // btnLoginSettings
            // 
            this.btnLoginSettings.Caption = "بيانات الدخول";
            this.btnLoginSettings.Id = 94;
            this.btnLoginSettings.ImageUri.Uri = "Forward";
            this.btnLoginSettings.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnLoginSettings.ItemAppearance.Normal.Options.UseFont = true;
            this.btnLoginSettings.Name = "btnLoginSettings";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "المستخدمين";
            this.barButtonItem1.Id = 95;
            this.barButtonItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            this.barButtonItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barWorkspaceMenuItem1
            // 
            this.barWorkspaceMenuItem1.Caption = "                                                                        ";
            this.barWorkspaceMenuItem1.Id = 97;
            this.barWorkspaceMenuItem1.Name = "barWorkspaceMenuItem1";
            // 
            // barWorkspaceMenuItem2
            // 
            this.barWorkspaceMenuItem2.Caption = "barWorkspaceMenuItem2";
            this.barWorkspaceMenuItem2.Id = 98;
            this.barWorkspaceMenuItem2.Name = "barWorkspaceMenuItem2";
            // 
            // barSubItem22
            // 
            this.barSubItem22.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.barSubItem22.Caption = "الطلاب";
            this.barSubItem22.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem22.Glyph")));
            this.barSubItem22.Id = 102;
            this.barSubItem22.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 12F);
            this.barSubItem22.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem22.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem22)});
            this.barSubItem22.Name = "barSubItem22";
            this.barSubItem22.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "تغيير مجموعة طالب";
            this.barButtonItem21.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem21.Glyph")));
            this.barButtonItem21.Id = 103;
            this.barButtonItem21.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem21.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem21.Name = "barButtonItem21";
            this.barButtonItem21.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "المدينيين";
            this.barButtonItem22.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem22.Glyph")));
            this.barButtonItem22.Id = 104;
            this.barButtonItem22.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem22.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem22.Name = "barButtonItem22";
            this.barButtonItem22.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 314;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // navBarGroup4
            // 
            this.navBarGroup4.Caption = "navBarGroup4";
            this.navBarGroup4.Name = "navBarGroup4";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.RightToLeftLayout = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabbedMdiManager1.UseFormIconAsPageImage = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabbedMdiManager1.SelectedPageChanged += new System.EventHandler(this.xtraTabbedMdiManager1_SelectedPageChanged);
            this.xtraTabbedMdiManager1.PageRemoved += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.xtraTabbedMdiManager1_PageRemoved);
            // 
            // navBarItem1
            // 
            this.navBarItem1.Name = "navBarItem1";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 16F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Location = new System.Drawing.Point(363, 78);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(542, 399);
            this.groupControl1.TabIndex = 22;
            this.groupControl1.Text = "مجموعات اليوم";
            this.groupControl1.Visible = false;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 42);
            this.gridControl1.MainView = this.cardView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(538, 355);
            this.gridControl1.TabIndex = 15;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.cardView1,
            this.gridView2,
            this.gridView1});
            this.gridControl1.Visible = false;
            // 
            // cardView1
            // 
            this.cardView1.Appearance.Card.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardView1.Appearance.Card.Options.UseFont = true;
            this.cardView1.Appearance.CardCaption.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardView1.Appearance.CardCaption.Options.UseFont = true;
            this.cardView1.Appearance.FieldCaption.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardView1.Appearance.FieldCaption.Options.UseFont = true;
            this.cardView1.Appearance.FieldValue.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardView1.Appearance.FieldValue.Options.UseFont = true;
            this.cardView1.CardCaptionFormat = "المجموعة {0}";
            this.cardView1.FocusedCardTopFieldIndex = 0;
            this.cardView1.GridControl = this.gridControl1;
            this.cardView1.Name = "cardView1";
            this.cardView1.OptionsBehavior.Editable = false;
            this.cardView1.DoubleClick += new System.EventHandler(this.cardView1_DoubleClick);
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // frmMain
            // 
            this.Appearance.ForeColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseFont = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(18F, 35F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1278, 517);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.LookAndFeel.SkinName = "Office 2010 Blue";
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "frmMain";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Teaching Activities";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private DevExpress.XtraNavBar.NavBarGroup navBarGroup4;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem btnBackup;
        private DevExpress.XtraBars.BarButtonItem btnRestore;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarSubItem btnEditDates;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem17;
        private DevExpress.XtraBars.BarButtonItem barButtonItem18;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.XtraBars.BarSubItem barSubItem6;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem3;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem1;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem7;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem2;
        private DevExpress.XtraBars.BarButtonItem btnShowC1;
        private DevExpress.XtraBars.BarSubItem barSubItem8;
        private DevExpress.XtraBars.BarSubItem barSubItem9;
        private DevExpress.XtraBars.BarSubItem barSubItem10;
        private DevExpress.XtraBars.BarSubItem barSubItem11;
        private DevExpress.XtraBars.BarSubItem barSubItem12;
        private DevExpress.XtraBars.BarButtonItem btnShowC6;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem13;
        private DevExpress.XtraBars.BarSubItem barSubItem14;
        private DevExpress.XtraBars.BarButtonItem btnEditGroup;
        private DevExpress.XtraBars.BarSubItem barSubItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarSubItem barSubItem17;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarSubItem barSubItem16;
        private DevExpress.XtraBars.BarButtonItem btnMonthlyReport;
        private DevExpress.XtraBars.BarSubItem barSubItem18;
        private DevExpress.XtraBars.BarButtonItem btnAddTeacher;
        private DevExpress.XtraBars.BarButtonItem btnPrintingSettings;
        private DevExpress.XtraBars.BarButtonItem btnCenterData;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem19;
        private DevExpress.XtraBars.BarButtonItem btnAddGroup;
        private DevExpress.XtraBars.BarButtonItem btnShowGroups;
        private DevExpress.XtraBars.BarSubItem barSubItem20;
        private DevExpress.XtraBars.BarButtonItem btnChangeGroup;
        private DevExpress.XtraBars.BarButtonItem btnDepits;
        private DevExpress.XtraBars.BarButtonItem btnLoginSettings;
        private DevExpress.XtraBars.BarSubItem barSubItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarWorkspaceMenuItem barWorkspaceMenuItem1;
        private DevExpress.XtraBars.BarSubItem barUser;
        private DevExpress.XtraBars.BarWorkspaceMenuItem barWorkspaceMenuItem2;
        private DevExpress.XtraBars.BarButtonItem btnAddUser;
        private DevExpress.XtraBars.BarButtonItem btnEditUser;
        private DevExpress.XtraBars.BarSubItem barSubItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem23;
        private DevExpress.XtraBars.BarButtonItem btnIncome;
        private DevExpress.XtraBars.BarButtonItem btnChangePassword;
        private DevExpress.XtraBars.BarButtonItem btnLogout;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Card.CardView cardView1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    }
}